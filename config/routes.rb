Rails.application.routes.draw do
  scope :api, defaults: { format: 'json' }, module: 'api' do
    %w[v1].each do |version|
      namespace version do
        post 'auth/register', to: 'registrations#create'
        post 'auth/login', to: 'sessions#create'

        resources :friends, except: %I[update create] do
          collection do
            get 'find', to: 'friends#find'
          end
          member do
            post 'request', to: 'friends#create'
            post 'response', to: 'friends#verify'
          end
        end

        resources :metrics, only: :create

        get ':id/zones', to: 'geo_zones#index'
        post ':id/zones', to: 'geo_zones#create'
        delete ':id/zones', to: 'geo_zones#destroy'

        get 'tracks/:id', to: 'tracks#index'

        post 'token', to: 'tokens#create'

        get 'settings', to: 'settings#show'
      end
    end
  end

  mount ActionCable.server => '/cable'
  match '*unmatched_route', to: 'application#show_404!', via: :all
end
