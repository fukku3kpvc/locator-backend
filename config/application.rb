require_relative 'boot'

require "rails"
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"

Bundler.require(*Rails.groups)

module LocatorBackend
  class Application < Rails::Application
    config.eager_load_paths += Dir["#{Rails.root}/lib/**/"]
    config.autoload_paths += Dir[Rails.root.join(config.root, 'lib', '**/')]
    config.autoload_paths += Dir[Rails.root.join(config.root, 'lib', 'patches')]
    config.enable_dependency_loading = true

    config.i18n.default_locale = :ru
    config.i18n.fallbacks = %i[ru en]

    config.load_defaults 5.2
    config.time_zone = 'Europe/Moscow'

    config.log_tags = [
      ->(_) { Process.pid },
      lambda { |_|
        time = Time.now
        time.strftime('%Y-%m-%d %H:%M:%S') + " timing:#{time.to_f}"
      },
      ->(r) { r.uuid[0..7] }
    ]

    config.active_record.schema_format = :sql
    config.api_only = true
  end
end
