class ServicesContainer
  extend Dry::Container::Mixin

  register 'expo_push_client' do
    ::Clients::ExpoPush::Client.new
  end
end

module Services
  Import = Dry::AutoInject(ServicesContainer)
end
