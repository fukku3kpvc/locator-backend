Geocoder.configure(
  timeout: 5,                           # geocoding service timeout (secs)
  lookup: :yandex,                      # name of geocoding service (symbol)
  cache: ::Services::RedisClient.redis, # cache object (must respond to #[], #[]=, and #del)
  units: :km,                           # :km for kilometers or :mi for miles
  api_key: Settings.geocoder.key,       # API key for geocoding service
  language: :ru,                        # ISO-639 language code
  cache_prefix: 'geocoder:'             # prefix (string) to use for all cache keys

  # ip_lookup: :ipinfo_io,      # name of IP address geocoding service (symbol)
  # use_https: false,           # use HTTPS for lookup requests? (if supported)
  # http_proxy: nil,            # HTTP proxy server (user:pass@host:port)
  # https_proxy: nil,           # HTTPS proxy server (user:pass@host:port)

  # Exceptions that should not be rescued by default
  # (if you want to implement custom error handling);
  # supports SocketError and Timeout::Error
  # always_raise: [],

  # Calculation options
  # distances: :linear          # :spherical or :linear
)
