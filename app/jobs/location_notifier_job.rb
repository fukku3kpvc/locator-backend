class LocationNotifierJob < ApplicationJob
  queue_as :default

  # location = {
  #   user_id: Integer,
  #   longitude: Float,
  #   latitude: Float,
  #   address: String,
  #   local_time: DateTime
  # }
  def perform(location)
    RL.tagged_with_uid self.class.name.demodulize do
      user = User.find(location[:user_id])

      LocationsChannel.broadcast_to(user, location)
    end
  end
end
