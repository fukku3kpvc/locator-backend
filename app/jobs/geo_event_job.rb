class GeoEventJob < ApplicationJob

  queue_as :default

  def perform(user, current_point, previous_point)
    RL.tagged_with_uid self.class.name.demodulize do
      result = ::GeoZones::DetectChanges.new.call(
        user: user, current_point: build_point(current_point),
        previous_point: build_point(previous_point)
      )
      RL.info("GeoZones::DetectChanges result: #{result.value_or(result.failure)}")

      iterate_zones(result.value!)
    end
  end

  private

  def build_point(point)
    RGeo::Geographic.spherical_factory(srid: 4326).point(point[:lon], point[:lat])
  end

  def iterate_zones(event_zones)
    event_zones[:left].each { |zone| notify_watcher(zone, :left) }
    event_zones[:entered].each { |zone| notify_watcher(zone, :entered) }
  end

  def notify_watcher(zone, type)
    watcher = zone.watcher
    target = zone.target
    token = watcher.push_token
    return unless token.present?

    title = build_title(type)
    body = build_body(type, target.full_name, zone.name)

    ::Tokens::Notify.new.call(token: token.token, title: title, body: body)
  end

  def build_title(type)
    if type == :left
      Settings.notifications.left_zone.title
    else
      Settings.notifications.entered_zone.title
    end
  end

  def build_body(type, user_name, zone_name)
    if type == :left
      format(
        Settings.notifications.left_zone.body,
        user_name: user_name, zone_name: zone_name
      )
    else
      format(
        Settings.notifications.entered_zone.body,
        user_name: user_name, zone_name: zone_name
      )
    end
  end
end
