class LocationRepresenter < SymbolicHashRepresenter
  property :longitude, exec_context: :decorator
  property :latitude, exec_context: :decorator
  property :timestamp, exec_context: :decorator

  def longitude
    represented.lon
  end

  def latitude
    represented.lat
  end

  def timestamp
    represented.updated_at.iso8601
  end
end
