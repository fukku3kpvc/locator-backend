class InitiatedFriendshipRepresenter < SymbolicHashRepresenter
  property :id, exec_context: :decorator
  property :username, exec_context: :decorator
  property :name, exec_context: :decorator
  property :avatar, exec_context: :decorator
  property :verified, exec_context: :decorator
  property :allow_to_confirm, exec_context: :decorator
  property :location, exec_context: :decorator

  def id
    represented.recipient.id
  end

  def username
    represented.recipient.username
  end

  def name
    represented.recipient.full_name
  end

  def avatar
    represented.recipient.serialize_avatar
  end

  def verified
    represented.confirmed?
  end

  def allow_to_confirm
    false
  end

  def location
    friend = User.find(represented.recipient.id)
    friend_position = friend.locations.last
    if friend_position.present? && represented.confirmed?
      {
        longitude: friend_position.lon,
        latitude: friend_position.lat
      }
    end
  end
end
