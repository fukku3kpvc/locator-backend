class ReceivedFriendRepresenter < SymbolicHashRepresenter
  property :id, exec_context: :decorator
  property :telephone_number, exec_context: :decorator
  property :username, exec_context: :decorator
  property :name, exec_context: :decorator
  property :avatar, exec_context: :decorator
  property :metrics, exec_context: :decorator

  def id
    represented.initiator.id
  end

  def telephone_number
    represented.initiator.number
  end

  def username
    represented.initiator.username
  end

  def name
    represented.initiator.full_name
  end

  def avatar
    represented.initiator.serialize_avatar
  end

  def metrics
    out = {}

    if represented.initiator.metrics.last.present?
      out[:battery] = represented.initiator.metrics.last.battery
      out[:local_time] = represented.initiator.metrics.last.local_time

      if represented.initiator.locations.last.present?
        out[:location] = {}
        out[:location][:longitude] = represented.initiator.locations.last.lon
        out[:location][:latitude] = represented.initiator.locations.last.lat
        out[:location][:address] = represented.initiator.locations.last.address
      end
    end

    out.empty? ? nil : out
  end
end
