class UserSearchRepresenter < SymbolicHashRepresenter
  property :id
  property :username
  property :full_name, as: :name
  property :avatar, exec_context: :decorator

  def avatar
    represented.serialize_avatar
  end
end
