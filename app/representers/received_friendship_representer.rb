class ReceivedFriendshipRepresenter < SymbolicHashRepresenter
  property :id, exec_context: :decorator
  property :username, exec_context: :decorator
  property :name, exec_context: :decorator
  property :avatar, exec_context: :decorator
  property :verified, exec_context: :decorator
  property :allow_to_confirm, exec_context: :decorator
  property :location, exec_context: :decorator

  def id
    represented.initiator.id
  end

  def username
    represented.initiator.username
  end

  def name
    represented.initiator.full_name
  end

  def avatar
    represented.initiator.serialize_avatar
  end

  def verified
    represented.confirmed?
  end

  def allow_to_confirm
    represented.initiated?
  end

  def location
    friend = User.find(represented.initiator.id)
    friend_position = friend.locations.last
    if friend_position.present? && represented.confirmed?
      {
        longitude: friend_position.lon,
        latitude: friend_position.lat
      }
    end
  end
end
