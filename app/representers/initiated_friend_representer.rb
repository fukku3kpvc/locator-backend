class InitiatedFriendRepresenter < SymbolicHashRepresenter
  property :id, exec_context: :decorator
  property :telephone_number, exec_context: :decorator
  property :username, exec_context: :decorator
  property :name, exec_context: :decorator
  property :avatar, exec_context: :decorator
  property :metrics, exec_context: :decorator

  def id
    represented.recipient.id
  end

  def telephone_number
    represented.recipient.number
  end

  def username
    represented.recipient.username
  end

  def name
    represented.recipient.full_name
  end

  def avatar
    represented.recipient.serialize_avatar
  end

  def metrics
    out = {}

    if represented.recipient.metrics.last.present?
      out[:battery] = represented.recipient.metrics.last.battery
      out[:local_time] = represented.recipient.metrics.last.local_time

      if represented.recipient.locations.last.present?
        out[:location] = {}
        out[:location][:longitude] = represented.recipient.locations.last.lon
        out[:location][:latitude] = represented.recipient.locations.last.lat
        out[:location][:address] = represented.recipient.locations.last.address
      end
    end

    out.empty? ? nil : out
  end
end
