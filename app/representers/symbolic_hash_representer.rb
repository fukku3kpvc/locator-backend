class SymbolicHashRepresenter < Representable::Decorator
  include Representable::Hash
  include Representable::Hash::AllowSymbols
end
