class GeoZoneRepresenter < SymbolicHashRepresenter
  property :id, as: :zone_id
  property :center, exec_context: :decorator
  property :radius
  property :name
  property :color

  def center
    {
      longitude: represented.lon,
      latitude: represented.lat
    }
  end
end
