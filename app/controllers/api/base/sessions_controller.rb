module Api
  module Base
    class SessionsController < ApplicationController
      skip_before_action :authenticate!

      # POST /auth/login
      def create
        result = Auth::Login.new.call(params.to_unsafe_h)
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end
    end
  end
end