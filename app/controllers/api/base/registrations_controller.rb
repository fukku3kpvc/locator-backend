module Api
  module Base
    class RegistrationsController < ApplicationController
      skip_before_action :authenticate!

      # POST /auth/register
      def create
        result = Auth::Register.new.call(params.to_unsafe_h)
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end
    end
  end
end