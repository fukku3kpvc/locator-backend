module Api
  module Base
    class TracksController < ApplicationController
      # GET /tracks/:id
      def index
        result = Tracks::Index.new.call(
          params.to_unsafe_h.merge(
            id: params[:id].to_i,
            user: current_user
          )
        )
        RL.info("Tracks::Index result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end
    end
  end
end
