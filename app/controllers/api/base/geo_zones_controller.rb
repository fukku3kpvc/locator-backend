module Api
  module Base
    class GeoZonesController < ApplicationController
      # GET /:id/zones
      def index
        result = GeoZones::Index.new.call(
          params.to_unsafe_h.merge(
            id: params[:id].to_i,
            user: current_user
          )
        )
        RL.info("GeoZones::Index result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end

      # POST /:id/zones
      def create
        result = GeoZones::Create.new.call(
          params.to_unsafe_h.merge(
            id: params[:id].to_i,
            user: current_user,
            radius: params[:radius].to_i
          )
        )
        RL.info("GeoZones::Create result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result
      end

      # DELETE /:id/zones
      def destroy
        result = GeoZones::Destroy.new.call(
          params.to_unsafe_h.merge(
            id: params[:id].to_i,
            user: current_user,
            zone_id: params[:zone_id].to_i
          )
        )
        RL.info("GeoZones::Destroy result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result
      end
    end
  end
end
