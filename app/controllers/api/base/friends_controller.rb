module Api
  module Base
    class FriendsController < ApplicationController
      # GET /friends
      def index
        result = Friend::Index.new.call(user: current_user)
        RL.info("Friend::Index result: #{result.value_or(result.failure)}")
        render_api_result(result.value!)
      end

      # GET /friends/find
      def find
        result = Friend::Search.new.call(params.to_unsafe_h.merge(user: current_user))
        RL.info("Friend::Search result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end

      # GET /friends/:id
      def show
        result = Friend::Show.new.call(params.to_unsafe_h.merge(user: current_user))
        RL.info("Friend::Show result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end

      # POST /friends/:id/request
      def create
        result = Friend::Create.new.call(params.to_unsafe_h.merge(user: current_user))
        RL.info("Friend::Create result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result
      end

      # POST /friends/:id/response
      def verify
        result = Friend::Verify.new.call(params.to_unsafe_h.merge(user: current_user))
        RL.info("Friend::Verify result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result
      end

      # DELETE /friends/:id/destroy
      def destroy
        result = Friend::Destroy.new.call(params.to_unsafe_h.merge(user: current_user))
        RL.info("Friend::Destroy result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result
      end
    end
  end
end
