module Api
  module Base
    class SettingsController < ApplicationController
      # GET /settings
      def show
        result = UserSettings::Show.new.call(
          params.to_unsafe_h.merge(
            user: current_user
          )
        )
        RL.info("UserSettings::Show result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end
    end
  end
end
