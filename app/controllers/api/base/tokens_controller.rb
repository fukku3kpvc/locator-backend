module Api
  module Base
    class TokensController < ApplicationController
      # POST /token
      def create
        result = Tokens::Create.new.call(
          params.to_unsafe_h.merge(
            user: current_user
          )
        )
        RL.info("Tokens::Create result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result(result.value!)
      end
    end
  end
end
