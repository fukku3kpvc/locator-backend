module Api
  module Base
    class MetricsController < ApplicationController
      # POST /metrics
      def create
        result = Metrics::Create.new.call(
          params.to_unsafe_h.merge(
            user: current_user,
            battery: params[:battery].to_i
          )
        )
        RL.info("Metric::Create result: #{result.value_or(result.failure)}")
        raise ActionError, result.failure if result.failure?

        render_api_result
      end
    end
  end
end
