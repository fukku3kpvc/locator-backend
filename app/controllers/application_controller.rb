class ApplicationController < ActionController::API
  include ::Api::Base::Helpers
  include ::Api::Base::Errors
  include ::Api::Base::Authentication

  skip_before_action :authenticate!, if: -> { controller_name == 'application' }

  rescue_from ActionError do |err|
    render json: err.to_h
  end

  def show_404!
    head :not_found
  end

  # rescue_from ActiveRecord::RecordNotFound do |err|
  #   render json: ::Errors::WrongId.new(id: err.id).to_h
  # end
end
