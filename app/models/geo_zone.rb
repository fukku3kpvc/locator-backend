class GeoZone < ApplicationRecord
  MIN_RADIUS = 100
  MAX_RADIUS = 1_000

  belongs_to :friendship
  belongs_to :watcher, class_name: 'User'
  belongs_to :target, class_name: 'User'

  validate :friendship_participants
  validates :center, :radius, :name, :color, presence: true
  validates_inclusion_of :radius, in: MIN_RADIUS..MAX_RADIUS

  scope :containing, lambda { |lon, lat|
    condition = 'ST_DWithin(center, ST_SetSRID(ST_MakePoint(?, ?), 4326), radius)'
    where(condition, lon, lat)
  }

  def lat
    center.y
  end

  def lon
    center.x
  end

  alias latitude lat
  alias longitude lon

  private

  def friendship_participants
    e = I18n.t!('errors.zone_participants')
    f = I18n.t!('errors.zone_friendship')
    participants = [friendship.initiator.id, friendship.recipient.id]
    errors.add(:friendship, f) unless friendship.confirmed?
    errors.add(:target, e) unless participants.include?(target.id)
    errors.add(:watcher, e) unless participants.include?(watcher.id)
  end
end

# == Schema Information
#
# Table name: geo_zones
#
#  id            :bigint           not null, primary key
#  friendship_id :bigint           not null
#  watcher_id    :bigint           not null
#  target_id     :bigint           not null
#  center        :geography({:srid not null, point, 4326
#  radius        :integer          not null
#  name          :string           not null
#  color         :string           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
