class Friendship < ApplicationRecord
  include AASM

  belongs_to :initiator, class_name: 'User'
  belongs_to :recipient, class_name: 'User'
  has_many :geo_zones, dependent: :destroy

  validate :uniqueness_of_instance, :identical_participants, on: :create

  # scope :relevant, -> { includes(:initiator, :recipient).where.not(state: 'rejected') }
  scope :relevant, -> { where.not(state: 'rejected') }

  aasm column: :state do
    state :initiated, initial: true
    state :confirmed
    state :rejected

    event :reject do
      transitions from: :initiated, to: :rejected
    end

    event :confirm do
      after { update(available_at: Time.zone.now) }

      transitions from: :initiated, to: :confirmed
    end
  end

  private

  def uniqueness_of_instance
    e = I18n.t!('errors.uniquness')
    errors.add(:id, e) if Friendship.find_by(initiator: initiator, recipient: recipient)
    errors.add(:id, e) if Friendship.find_by(initiator: recipient, recipient: initiator)
  end

  def identical_participants
    e = I18n.t!('errors.identical_participants')
    errors.add(:id, e) if initiator_id == recipient_id
  end
end

# == Schema Information
#
# Table name: friendships
#
#  id           :bigint           not null, primary key
#  initiator_id :bigint           not null
#  recipient_id :bigint           not null
#  state        :enum             default("initiated"), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  available_at :datetime
#
