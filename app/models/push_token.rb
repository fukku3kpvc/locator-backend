class PushToken < ApplicationRecord
  belongs_to :user

  validates_presence_of :os, :token
end

# == Schema Information
#
# Table name: push_tokens
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  os         :enum             not null
#  token      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
