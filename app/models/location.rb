class Location < ApplicationRecord
  BUFFER_SIZE = 100

  belongs_to :user

  validates_presence_of :position, :parsed_address, :address

  scope :between, lambda { |from, to|
    start_date = Time.zone.parse(from).beginning_of_day
    end_date = Time.zone.parse(to).end_of_day

    where(updated_at: start_date..end_date)
  }

  def lat
    position.y
  end

  def lon
    position.x
  end

  alias latitude lat
  alias longitude lon
end

# == Schema Information
#
# Table name: locations
#
#  id             :bigint           not null, primary key
#  user_id        :bigint           not null
#  parsed_address :jsonb            not null
#  address        :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  position       :geography({:srid not null, point, 4326
#
