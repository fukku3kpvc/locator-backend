class Metric < ApplicationRecord
  belongs_to :user

  validates :battery, numericality: { only_integer: true,
                                      greater_than: 0,
                                      less_than: 101 }
  validates_presence_of :battery, :time_zone

  def local_time
    updated_at.in_time_zone(time_zone).iso8601
  end
end

# == Schema Information
#
# Table name: metrics
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  battery    :integer          not null
#  time_zone  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
