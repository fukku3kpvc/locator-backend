class User < ApplicationRecord
  include PgSearch::Model

  PASSWORD_MIN_SIZE = 6

  mount_base64_uploader :avatar, AvatarUploader
  has_secure_password
  pg_search_scope :search,
                  against: %I[username full_name],
                  using: {
                    tsearch: {
                      any_word: true
                    },
                    trigram: {
                      word_similarity: true
                    }
                  }

  validates :username, :number, uniqueness: true

  has_one :push_token, dependent: :destroy

  has_many :friendships, ->(user) { unscope(:where).where('recipient_id = :id OR initiator_id = :id', id: user.id) }

  has_many :initiated_friendships,
           class_name: 'Friendship',
           foreign_key: 'initiator_id',
           dependent: :destroy

  has_many :received_friendships,
           class_name: 'Friendship',
           foreign_key: 'recipient_id',
           dependent: :destroy

  has_many :recipients, through: :initiated_friendships, class_name: 'User'
  has_many :initiators, through: :received_friendships, class_name: 'User'

  has_many :locations, dependent: :destroy
  has_many :metrics, dependent: :destroy

  has_many :watcher_geo_zones,
           class_name: 'GeoZone',
           foreign_key: 'watcher_id',
           dependent: :destroy

  has_many :target_geo_zones,
           class_name: 'GeoZone',
           foreign_key: 'target_id',
           dependent: :destroy

  def friends
    user_ids = friendships.pluck(:recipient_id, :initiator_id).flatten
    user_ids = user_ids.uniq - [id]
    User.where(id: user_ids)
  end

  def find_friendship_with(friend)
    initiator_friendship = initiated_friendships.find_by(recipient: friend)
    recipient_friendship = received_friendships.find_by(initiator: friend)

    initiator_friendship || recipient_friendship
  end

  def serialize_avatar
    return if avatar.file.nil?

    avatar_data = File.open(avatar.thumb.current_path).read
    avatar_base64 = Base64.strict_encode64(avatar_data)

    "data:#{avatar.content_type};base64, #{avatar_base64}"
  end
end

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  number          :bigint           not null
#  username        :string           not null
#  full_name       :string           not null
#  avatar          :string
#  password_digest :string           default(""), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
