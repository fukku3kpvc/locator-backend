module ApplicationCable
  class Connection < ActionCable::Connection::Base
    AUTH_PARAM = 'SESSION-TOKEN'.to_sym.freeze

    identified_by :current_user

    def connect
      RL.tagged_with_uid 'SOCKET::CONNECTION' do
        self.current_user = determine_user
        RL.info("Received connection from User ID: #{self.current_user.id}")
      end
    end

    private

    def determine_user
      RL.info("Received connection request, params: #{request.params}, headers: #{request.headers}")
      token = request.params[AUTH_PARAM]
      raise_session_error unless token

      decoded_token = JWT.decode(token, nil, false)
      user_id = decoded_token.first['user_id']
      raise_session_error unless user_id

      User.find(user_id)
    rescue JWT::DecodeError, ActiveRecord::RecordNotFound => e
      RL.error("ApplicationCable::Connection error, details: #{e}")
      raise_session_error
    end

    def raise_session_error
      reject_unauthorized_connection
    end
  end
end
