class LocationsChannel < ApplicationCable::Channel
  def subscribed
    friend = determine_friend

    if friend.present?
      stream_for friend
      RL.info("Start streaming for friend ID: #{friend.id}, consumer ID: #{current_user.id}")
    else
      reject
      RL.info("Reject connection for consumer ID: #{current_user.id}")
    end
  end

  def unsubscribed
    RL.info("Stop all streams for consumer ID: #{current_user.id}")
    stop_all_streams
  end

  private

  def determine_friend
    current_user.friends.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    RL.error("Could not find friend with id #{e.id}")
    nil
  end
end
