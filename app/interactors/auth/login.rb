module Auth
  class Login < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:login).value(:str?)
      required(:password).value(:str?)
    end

    step :validate_input
    step :find_user
    step :authenticate
    map :generate_token

    def find_user(input)
      input[:user] = User.find_by!('username = ? OR number = ?',
                                   input[:login],
                                   input[:login].to_i)

      Success(input)
    rescue ActiveRecord::RecordNotFound
      Failure(WrongData.new.to_h)
    end

    def authenticate(input)
      if input[:user].authenticate(input[:password])
        Success(input)
      else
        Failure(WrongData.new.to_h)
      end
    end

    def generate_token(input)
      token = { user_id: input[:user].id }

      { token: JWT.encode(token, nil, 'none') }
    end
  end
end