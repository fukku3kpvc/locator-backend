module Auth
  class Register < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:telephone_number).value(:integer_convertible?)
      required(:username).filled(:str?)
      required(:name).filled(:str?)
      required(:password).filled(:str?, min_size?: User::PASSWORD_MIN_SIZE)
      optional(:avatar)
    end

    step :validate_input
    tee :build_user
    step :validate_user
    map :create_user

    def build_user(input)
      input[:user] = User.new(number: input[:telephone_number],
                              username: input[:username],
                              full_name: input[:name],
                              avatar: input[:avatar],
                              password: input[:password])
    end

    def validate_user(input)
      if input[:user].valid?
        Success(input)
      else
        Failure(ValidationFailed.new(
          input[:user].errors.keys.first,
          input[:user].errors.full_messages.to_sentence
        ).to_h)
      end
    end

    def create_user(input)
      input[:user].save!
      token = { user_id: input[:user].id }

      { token: JWT.encode(token, nil, 'none') }
    end
  end
end