module Friend
  class Verify < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).value(:integer_convertible?)
      required(:verified).value(:bool_convertible?)
      required(:user).value(type?: User)
    end

    step :validate_input
    step :find_friendship
    map :verify_friend

    def find_friendship(input)
      friend_exist = input[:user].friends.pluck(:id).include?(input[:id].to_i)
      return Failure(WrongID.new.to_h) unless friend_exist

      input[:friendship] = input[:user].received_friendships
                                       .find_by(initiator_id: input[:id])
      return Failure(WrongID.new.to_h) unless input[:friendship]
      return Failure(WrongID.new.to_h) unless input[:friendship].initiated?

      Success(input)
    end

    def verify_friend(input)
      if input[:verified] == 'true'
        input[:friendship].confirm!
        input[:friendship].update(available_at: Time.zone.now)
      else
        input[:friendship].reject!
      end
    end
  end
end
