module Friend
  class Index < ApplicationInteractor
    tee :collect_initiated_friendships
    tee :collect_received_friendships
    map :order_friendships

    def collect_initiated_friendships(input)
      initiated_friendships = User.includes(initiated_friendships: :recipient)
                                  .find(input[:user].id)
                                  .initiated_friendships
                                  .includes(recipient: :locations)
                                  .relevant

      input[:initiated] = InitiatedFriendshipRepresenter.for_collection
                                                        .new(initiated_friendships)
                                                        .to_hash
    end

    def collect_received_friendships(input)
      received_friendships = User.includes(received_friendships: :initiator)
                                 .find(input[:user].id)
                                 .received_friendships
                                 .includes(initiator: :locations)
                                 .relevant

      input[:received] = ReceivedFriendshipRepresenter.for_collection
                                                      .new(received_friendships)
                                                      .to_hash
    end

    def order_friendships(input)
      input[:friendships] = input[:initiated] + input[:received]
      input[:friendships].sort { |this, prev| prev['id'] <=> this['id'] }
    end
  end
end
