module Friend
  class Create < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).value(:integer_convertible?)
      required(:user).value(type?: User)
    end

    step :validate_input
    step :add_to_friends

    def add_to_friends(input)
      friend_exist = input[:user].friends.pluck(:id).include?(input[:id].to_i)
      friend = User.find(input[:id])
      return Failure(WrongID.new.to_h) if friend_exist

      friendship = Friendship.new(initiator: input[:user], recipient: friend)
      return Failure(WrongID.new.to_h) unless friendship.valid?

      friendship.save
      Success(friendship)
    rescue ActiveRecord::RecordNotFound
      Failure(WrongID.new.to_h)
    end
  end
end