module Friend
  class Destroy < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).value(:integer_convertible?)
      required(:user).value(type?: User)
    end

    step :validate_input
    step :find_friendship
    map :destroy_friendship

    def find_friendship(input)
      friend_exist = input[:user].friends.pluck(:id).include?(input[:id].to_i)
      return Failure(WrongID.new.to_h) unless friend_exist

      input[:initiated] = input[:user].initiated_friendships
                                      .confirmed
                                      .find_by(recipient_id: input[:id])

      input[:received] = input[:user].received_friendships
                                     .confirmed
                                     .find_by(initiator_id: input[:id])

      return Failure(WrongID.new.to_h) unless input[:initiated] || input[:received]

      Success(input)
    end

    def destroy_friendship(input)
      if input[:initiated]
        input[:initiated].destroy!
      else
        input[:received].destroy!
      end
    end
  end
end
