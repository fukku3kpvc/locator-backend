module Friend
  class Show < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).value(:integer_convertible?)
      required(:user).value(type?: User)
    end

    step :validate_input
    step :find_friend
    map :represent_friend

    def find_friend(input)
      friend_exist = input[:user].friends.pluck(:id).include?(input[:id].to_i)
      return Failure(WrongID.new.to_h) unless friend_exist

      input[:initiated] = User.find(input[:user].id)
                              .initiated_friendships
                              .relevant
                              .includes(:recipient)
                              .find_by(recipient_id: input[:id])

      input[:received] = User.find(input[:user].id)
                             .received_friendships
                             .relevant
                             .includes(:initiator)
                             .find_by(initiator_id: input[:id])

      return Failure(WrongID.new.to_h) unless input[:initiated] || input[:received]

      Success(input)
    end

    def represent_friend(input)
      if input[:initiated]
        InitiatedFriendRepresenter.new(input[:initiated]).to_hash
      else
        ReceivedFriendRepresenter.new(input[:received]).to_hash
      end
    end
  end
end
