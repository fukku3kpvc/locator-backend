module Friend
  class Search < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:search_name).filled(:str?)
      required(:user).value(type?: User)
    end

    step :validate_input
    map :find_users

    def find_users(input)
      search_result = User.search(input[:search_name]) - [input[:user]]
      UserSearchRepresenter.for_collection.new(search_result).to_hash
    end
  end
end
