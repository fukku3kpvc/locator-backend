module Tokens
  class Notify < ApplicationInteractor
    include ::Services::Import['expo_push_client']

    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:token).filled(:str?)
      required(:title).filled(:str?)
      required(:body).filled(:str?)
    end

    map :notify

    def notify(input)
      expo_push_client.send_push(
        input[:token], input[:title], input[:body]
      )
    end
  end
end
