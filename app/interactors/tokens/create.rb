module Tokens
  class Create < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:user).value(type?: User)
      required(:token).filled(:str?)
      required(:os).value(included_in?: %w[android ios])
    end

    step :validate_input
    map :update_token_info

    def update_token_info(input)
      input[:push_token] = input[:user].push_token

      if input[:push_token].present?
        input[:push_token].update(os: input[:os], token: input[:token])
      else
        input[:user].create_push_token(os: input[:os], token: input[:token])
      end
    end
  end
end
