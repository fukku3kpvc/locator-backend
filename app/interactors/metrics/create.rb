module Metrics
  class Create < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:user).value(type?: User)
      required(:battery).filled(:int?, gt?: 0, lteq?: 100)
      required(:datetime).filled(:str?)
      required(:location).schema do
        required(:longitude).value(:coordinates?)
        required(:latitude).value(:coordinates?)
      end
    end

    step :validate_input
    step :detect_time_zone
    tee :check_distance
    tee :perform_geo_event_job
    tee :detect_address
    tee :save_location
    tee :save_metric

    def detect_time_zone(input)
      numerator = Time.iso8601(input[:datetime]).to_datetime.offset.numerator
      input[:tz] = ActiveSupport::TimeZone[numerator].tzinfo.name
      Success(input)
    rescue ArgumentError
      Failure(WrongFormat.new('datetime', 'should be in ISO8601').to_h)
    end

    def check_distance(input)
      input[:create_location] = true
      input[:current_point] = RGeo::Geographic.spherical_factory(srid: 4326)
                                              .point(
                                                input[:location][:longitude],
                                                input[:location][:latitude]
                                              )
      last_point = input[:user].locations.last&.position

      return input[:create_location] unless last_point.present?

      input[:previous_point] = RGeo::Geographic.spherical_factory(srid: 4326)
                                               .point(
                                                 last_point.lon,
                                                 last_point.lat
                                               )

      input[:create_location] =
        input[:current_point].distance(last_point).round > Location::BUFFER_SIZE
    end

    def perform_geo_event_job(input)
      return unless input[:create_location] && input[:previous_point].present?

      current = { lon: input[:current_point].lon, lat: input[:current_point].lat }
      prev = { lon: input[:previous_point].lon, lat: input[:previous_point].lat }

      GeoEventJob.perform_later(input[:user], current, prev)
    end

    def detect_address(input)
      if input[:create_location]
        gco_result = Geocoder.search([input[:location][:latitude], input[:location][:longitude]])
        input[:parsed_address] = gco_result.first.data.deep_symbolize_keys!
        input[:address] = input[:parsed_address].dig(
          :GeoObject, :metaDataProperty, :GeocoderMetaData, :Address, :formatted
        )
      end
    end

    def save_location(input)
      if input[:create_location]
        input[:user].locations.create(
          position: input[:current_point],
          parsed_address: input[:parsed_address],
          address: input[:address]
        )

        location = {
          user_id: input[:user].id,
          longitude: input[:current_point].lon,
          latitude: input[:current_point].lat,
          address: input[:address],
          local_time: input[:datetime]
        }
        LocationNotifierJob.perform_later(location)
      else
        input[:user].locations.last.touch
      end
    end

    def save_metric(input)
      if input[:user].metrics.last.nil? ||
         input[:user].metrics.last&.battery.to_i != input[:battery].to_i

        input[:user].metrics.create(
          battery: input[:battery],
          time_zone: input[:tz]
        )
      elsif input[:user].metrics.last.present?
        input[:user].metrics.last.touch
      end
    end
  end
end
