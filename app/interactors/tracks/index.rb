module Tracks
  class Index < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:user).value(type?: User)
      required(:id).filled(:int?)
      required(:from).filled(:str?)
      required(:to).filled(:str?)
    end

    step :validate_input
    step :find_friend, with: 'operations.find_friend'
    map :list_points

    def list_points(input)
      points = input[:friend].locations
                             .between(input[:from], input[:to])
                             .order(:updated_at)

      LocationRepresenter.for_collection.new(points).to_hash
    end
  end
end
