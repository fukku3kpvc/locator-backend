require 'dry/transaction/operation'

module Operations
  class FindFriend
    include Dry::Transaction::Operation
    include ::Api::Base::Errors

    def call(input)
      friend_exist = input[:user].friends.pluck(:id).include?(input[:id].to_i)
      return Failure(WrongID.new.to_h) unless friend_exist

      input[:friend] = User.find(input[:id])
      input[:friendship] = input[:user].find_friendship_with(input[:friend])
      if input[:friendship].nil? || !input[:friendship]&.confirmed?
        return Failure(WrongID.new.to_h)
      end

      Success(input)
    end
  end
end
