module Operations
  class Container
    extend Dry::Container::Mixin

    namespace :operations do
      register :find_friend do
        ::Operations::FindFriend.new
      end
    end
  end
end
