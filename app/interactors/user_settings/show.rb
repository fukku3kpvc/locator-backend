module UserSettings
  class Show < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:user).value(type?: User)
    end

    step :validate_input
    tee :pluck_user_info
    tee :merge_last_location
    map :merge_app_version

    def pluck_user_info(input)
      input[:data] = {
        username: input[:user].username,
        name: input[:user].full_name,
        telephone_number: input[:user].number,
        avatar: input[:user].serialize_avatar,
        friends_count: input[:user].friends.count,
        geo_zones_count: input[:user].watcher_geo_zones.count,
        locations_count: input[:user].locations.count
      }
    end

    def merge_last_location(input)
      location = input[:user].locations.last

      return unless location.present?

      input[:data].merge!(
        last_location: {
          longitude: location.lon,
          latitude: location.lat,
          address: location.address,
          local_time: location.updated_at.iso8601
        }
      )
    end

    def merge_app_version(input)
      input[:data].merge!(app_version: Settings.app.version)

      input[:data]
    end
  end
end
