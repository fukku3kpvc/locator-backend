module GeoZones
  class Index < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).filled(:int?)
      required(:user).value(type?: User)
    end

    step :validate_input
    step :find_friend, with: 'operations.find_friend'
    map :list_zones

    def list_zones(input)
      zones = input[:user].watcher_geo_zones.where(
        target: input[:friend],
        friendship: input[:friendship]
      )

      GeoZoneRepresenter.for_collection.new(zones).to_hash
    end
  end
end
