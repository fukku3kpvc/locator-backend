module GeoZones
  class Destroy < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).filled(:int?)
      required(:user).value(type?: User)
      required(:zone_id).filled(:int?)
    end

    step :validate_input
    step :find_friend, with: 'operations.find_friend'
    step :find_zone
    map :destroy_zone

    def find_zone(input)
      input[:zone] = input[:user].watcher_geo_zones.find_by!(
        id: input[:zone_id],
        target: input[:friend],
        friendship: input[:friendship]
      )

      Success(input)
    rescue ActiveRecord::RecordNotFound
      Failure(WrongID.new.to_h)
    end

    def destroy_zone(input)
      input[:zone].destroy
    end
  end
end
