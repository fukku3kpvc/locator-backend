module GeoZones
  class Create < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:id).filled(:int?)
      required(:user).value(type?: User)
      required(:center).schema do
        required(:longitude).value(:coordinates?)
        required(:latitude).value(:coordinates?)
      end
      required(:radius).filled(:int?, gteq?: 100, lteq?: 1_000)
      required(:name).filled(:str?)
      required(:color).value(:hex?)
    end

    step :validate_input
    step :find_friend, with: 'operations.find_friend'
    tee :prepare_point
    tee :create_zone

    def prepare_point(input)
      input[:point] = RGeo::Geographic.spherical_factory(srid: 4326)
                                      .point(
                                        input[:center][:longitude],
                                        input[:center][:latitude]
                                      )
    end

    def create_zone(input)
      RL.info("Creating zone for User##{input[:user].id} and Friendship##{input[:friendship].id}")

      input[:zone] = GeoZone.create(
        watcher: input[:user],
        target: input[:friend],
        friendship: input[:friendship],
        center: input[:point],
        radius: input[:radius],
        name: input[:name],
        color: input[:color]
      )
    end
  end
end
