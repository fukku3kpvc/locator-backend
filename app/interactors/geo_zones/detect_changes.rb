module GeoZones
  class DetectChanges < ApplicationInteractor
    schema do
      configure do
        config.messages = :i18n
        predicates(CustomPredicates)
      end

      required(:user).value(type?: User)
      required(:current_point).value(:point?)
      required(:previous_point).value(:point?)
    end

    tee :find_zones
    map :generate_events

    def find_zones(input)
      input[:previous_zones] = input[:user].target_geo_zones
                                           .containing(
                                             input[:previous_point].lon,
                                             input[:previous_point].lat
                                           )

      input[:current_zones] = input[:user].target_geo_zones
                                          .containing(
                                            input[:current_point].lon,
                                            input[:current_point].lat
                                          )
    end

    def generate_events(input)
      {
        left: input[:previous_zones] - input[:current_zones],
        entered: input[:current_zones] - input[:previous_zones]
      }
    end
  end
end
