class ApplicationInteractor
  include Dry::Transaction
  include ::Api::Base::Errors
  include Dry::Transaction(container: ::Operations::Container)

  class << self
    attr_reader :input_schema

    def schema(schema = nil, &block)
      @input_schema = block_given? ? Dry::Validation.Schema(&block) : schema
    end

    def params_schema(&block)
      @input_schema = Dry::Validation.Params(&block) if block_given?
    end
  end

  def validate_input(input, error_class: nil)
    input = input.to_hash.deep_symbolize_keys!
    result = schema.call(input)
    return Success(result.to_h) if result.success?

    if error_class
      Failure(error_class.new.to_h)
    else
      field = result.errors.keys.first
      Failure(WrongFormat.new(field, result.errors[field].first).to_h)
    end
  end

  private

  def schema
    self.class.input_schema
  end
end
