# Локатор
Сервис по определению местоположения друзей.

## Installation

Сервис представляет из себя REST-API Ruby on Rails приложение.

Требуемая версия ruby указанна в файле .ruby-version
Список ruby-gems описан в Gemfile
В качестве хранилища данных сервис использует СУБД PostgreSQL, адаптер postgis
А так же в качестве in-memory-storage используется Redis (для веб сокетов)

Шаги установки:

* Склонировать репозиторий проекта
* Установить [rbenv](https://github.com/rbenv/rbenv) или [rvm](https://github.com/rvm/rvm)
* Установить зависимости:
    * Установить geos
    * Установить PostGis
    * Установить PostgreSQL

* Создать файл с настройками для поключения к бд на основе приведенного примера

  ```
  cp config/database.example.yml config/database.yml
  ```

* Для настройки БД нужно создать учетную запись суперпользователя,  
создать базы данных и выполнить миграции для development и test окружений:

  ```
  bundle exec rake db:create db:migrate db:seed
  RAILS_ENV=test bundle exec rake db:create db:migrate db:seed
  ```

* Запустить тесты, что бы убедиться что проект развернут правильно
  ```
  bundle exec rspec
  ```

## Работа с паролями через credentials

Все `секреты` ( пароли и/или любая секьюрная информация ) хранятся в зашифрованном виде в репозитории,
в файле `config/credentials.yml.enc`.

Для использования `секретов` в проекте - нужен файл с паролем.
Он должен лежать в той же директории, что и зашифрованный файл.
Формат файла должен быть следующим: `config/master.key`.

* `config/master.key` - нельзя хранить в репозитории

Для работы с `секретами` в терминале, используются следующие команды:

    редактирвание: EDITOR='nano' bundle exec rails credentials:edit
