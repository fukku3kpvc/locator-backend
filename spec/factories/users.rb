FactoryBot.define do
  factory :user do
    sequence(:number, 79000000000)
    username { FFaker::Internet.user_name }
    full_name { FFaker::Name.name }
    password { FFaker::Internet.password }

    trait :with_avatar do
      image = [0, 1].sample
      image_file = image.zero? ? 'image.jpg' : 'image_2.jpeg'
      image_type = image.zero? ? 'image/jpg' : 'image/jpeg'
      file = Rails.root.join('spec', 'fixtures', image_file)
      image = Base64.strict_encode64(File.open(file).read)
      avatar { "data:#{image_type};base64, #{image}" }
    end
  end
end
