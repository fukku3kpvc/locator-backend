FactoryBot.define do
  factory :push_token do
    association :user
    os { 'ios' }
    token { FFaker::Lorem.characters }
  end

  trait :ios do
    os { 'ios' }
  end

  trait :android do
    os { 'android' }
  end
end
