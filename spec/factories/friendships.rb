FactoryBot.define do
  factory :friendship do
    association :initiator, factory: :user
    association :recipient, factory: :user
  end

  trait :initiated do
    state { :initiated }
  end

  trait :confirmed do
    state { :confirmed }
    available_at { Time.zone.now }
  end

  trait :rejected do
    state { :rejected }
  end
end
