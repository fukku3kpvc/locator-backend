FactoryBot.define do
  factory :geo_zone do
    association :target, factory: :user
    association :watcher, factory: :user
    association :friendship
    center do
      RGeo::Geographic.spherical_factory(srid: 4326)
                      .point(54.725215, 55.940624)
    end
    radius { 300 }
    name { 'sample' }
    color { '#eee' }
  end
end
