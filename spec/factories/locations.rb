FactoryBot.define do
  factory :location do
    association :user
    position do
      RGeo::Geographic.spherical_factory(srid: 4326)
                      .point(54.725215, 55.940624)
    end
    parsed_address do
      {
        GeoObject:
         {
           metaDataProperty:
            {
              GeocoderMetaData:
               {
                 precision: 'exact',
                 text: 'Россия, Республика Башкортостан, Уфа, улица Карла Маркса, 12к6',
                 kind: 'house',
                 Address:
                  {
                    country_code: 'RU',
                    formatted: 'Россия, Республика Башкортостан, Уфа, улица Карла Маркса, 12к6',
                    postal_code: '450008',
                    Components:
                     [
                       {
                         kind: 'country', name: 'Россия'
                       },
                       {
                         kind: 'province', name: 'Приволжский федеральный округ'
                       },
                       {
                         kind: 'province', name: 'Республика Башкортостан'
                       },
                       {
                         kind: 'area', name: 'городской округ Уфа'
                       },
                       {
                         kind: 'locality', name: 'Уфа'
                       },
                       {
                         kind: 'street', name: 'улица Карла Маркса'
                       },
                       {
                         kind: 'house', name: '12к6'
                       }
                     ]
                  },
                 AddressDetails:
                  {
                    Country:
                     {
                       AddressLine: 'Россия, Республика Башкортостан, Уфа, улица Карла Маркса, 12к6',
                       CountryNameCode: 'RU',
                       CountryName: 'Россия',
                       AdministrativeArea:
                        {
                          AdministrativeAreaName: 'Республика Башкортостан',
                          SubAdministrativeArea:
                           {
                             SubAdministrativeAreaName: 'городской округ Уфа',
                             Locality:
                              {
                                LocalityName: 'Уфа',
                                Thoroughfare:
                                 {
                                   ThoroughfareName: 'улица Карла Маркса',
                                   Premise:
                                     {
                                       PremiseNumber: '12к6',
                                       PostalCode:
                                         {
                                           PostalCodeNumber: '450008'
                                         }
                                     }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            },
           name: 'улица Карла Маркса, 12к6',
           description: 'Уфа, Республика Башкортостан, Россия',
           boundedBy:
             {
               Envelope:
                 {
                   lowerCorner: '55.936817 54.722482',
                   upperCorner: '55.945028 54.727235'
                 }
             },
           Point:
             {
               pos: '55.940922 54.724859'
             }
         }
      }
    end
    address { 'Россия, Республика Башкортостан, Уфа, улица Карла Маркса, 12к6' }
  end
end
