FactoryBot.define do
  factory :metric do
    association :user
    battery { (1..100).to_a.sample }
    time_zone { 'Asia/Yekaterinburg' }
  end
end
