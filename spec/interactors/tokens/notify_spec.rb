require 'rails_helper'

describe Tokens::Notify do
  subject { described_class.new.call(input) }

  let!(:input) do
    {
      token: 'sample_token',
      title: 'hello',
      body: 'world'
    }
  end

  it 'calls expo_push_client' do
    expect_any_instance_of(Clients::ExpoPush::Client).to receive(:send_push)
      .with('sample_token', 'hello', 'world')

    subject
  end
end
