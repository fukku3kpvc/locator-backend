require 'rails_helper'

describe Auth::Register do
  subject { described_class.new.call(input) }

  let!(:input) do
    {
      telephone_number: 79000000000,
      username: 'tyler',
      name: 'Tyler The Compiler',
      password: 'secret'
    }
  end

  context 'valid input' do
    it 'returns success' do
      expect(subject).to be_success
    end

    it 'creates user' do
      expect { subject }.to change { User.count }.by 1
    end
  end

  context 'invalid input' do
    context 'wrong_format' do
      before { input[:password] = '' }

      it 'does not create user' do
        expect { subject }.not_to change { User.count }
      end

      it_behaves_like :returns_error_hash, 'wrong_format'
    end

    context 'validation_failed' do
      before { create(:user, number: 79000000000) }

      it 'does not create user' do
        expect { subject }.not_to change { User.count }
      end

      it_behaves_like :returns_error_hash, 'validation_failed'
    end
  end
end