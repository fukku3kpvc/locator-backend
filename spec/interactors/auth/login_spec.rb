require 'rails_helper'

describe Auth::Login do
  subject { described_class.new.call(input) }

  let!(:user) do
    create(:user, number: 79999999998, username: 'test', password: 'strong_pass')
  end
  let!(:input) do
    {
      login: '79999999998',
      password: 'strong_pass'
    }
  end

  context 'valid input' do
    context 'login is number' do
      it 'returns success' do
        expect(subject).to be_success
      end
    end

    context 'login is username' do
      before { input[:login] = 'test' }

      it 'returns success' do
        expect(subject).to be_success
      end
    end
  end

  context 'invalid input' do
    context 'wrong_format' do
      before { input[:login] = 0 }

      it_behaves_like :returns_error_hash, 'wrong_format'
    end

    context 'wrong_data' do
      before { input[:password] = 'wrong' }

      it_behaves_like :returns_error_hash, 'wrong_data'
    end
  end
end