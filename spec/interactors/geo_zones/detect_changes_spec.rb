require 'rails_helper'

describe GeoZones::DetectChanges do
  subject { described_class.new.call(input) }

  let!(:target) { create(:user) }
  let!(:watcher) { create(:user) }
  let!(:friendship) do
    create(:friendship, :confirmed, initiator: watcher, recipient: target)
  end
  let!(:center) do
    RGeo::Geographic.spherical_factory(srid: 4326)
                    .point(55.993222, 54.743822)
  end
  let!(:zone) do
    create(
      :geo_zone, center: center, radius: 100, watcher: watcher,
                 target: target, friendship: friendship
    )
  end

  let(:input) do
    {
      user: target,
      current_point: current_point,
      previous_point: previous_point
    }
  end

  context 'one zone' do
    context 'containing' do
      let!(:current_point) do
        RGeo::Geographic.spherical_factory(srid: 4326)
                        .point(55.99394, 54.743553)
      end

      context 'entered zone' do
        let!(:previous_point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.992487, 54.740456)
        end

        it 'returns zone in entered zone event' do
          expect(subject.value![:entered].size).to eq(1)
          expect(subject.value![:left].size).to eq(0)
          expect(subject.value![:entered].first).to eq(zone)
        end
      end

      context 'without any event' do
        let!(:previous_point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.993613, 54.743706)
        end

        it 'returns empty hash events' do
          expect(subject.value![:entered].size).to eq(0)
          expect(subject.value![:left].size).to eq(0)
        end
      end
    end

    context 'not containing' do
      let!(:current_point) do
        RGeo::Geographic.spherical_factory(srid: 4326)
                        .point(55.992487, 54.740456)
      end

      context 'left zone' do
        let!(:previous_point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.99394, 54.743553)
        end

        it 'returns zone in left zone event' do
          expect(subject.value![:entered].size).to eq(0)
          expect(subject.value![:left].size).to eq(1)
          expect(subject.value![:left].first).to eq(zone)
        end
      end

      context 'without any event' do
        let!(:previous_point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.992994, 54.739552)
        end

        it 'returns empty hash events' do
          expect(subject.value![:entered].size).to eq(0)
          expect(subject.value![:left].size).to eq(0)
        end
      end
    end
  end
end
