require 'rails_helper'

describe ApplicationCable::Connection, type: :channel do
  let(:user) { create(:user) }

  context 'with session token' do
    let!(:token) { JWT.encode({ user_id: user.id }, nil, 'none') }
    it 'confirms connection' do
      connect '/cable', params: { 'SESSION-TOKEN': token }
      expect(connection.current_user.id).to eq user.id
    end
  end

  context 'without session token' do
    it 'rejects connection' do
      expect { connect '/cable' }.to have_rejected_connection
    end
  end
end
