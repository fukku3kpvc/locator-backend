require 'rails_helper'

describe LocationsChannel, type: :channel do
  let!(:user) { create(:user) }
  before do
    stub_connection current_user: user
  end

  context 'with no friend ID' do
    it 'rejects subscription' do
      subscribe

      expect(subscription).to be_rejected
    end
  end

  context 'with friend ID' do
    let!(:friend) { create(:user) }
    let!(:friendship) do
      create(:friendship, :confirmed, initiator: user, recipient: friend)
    end

    context 'as valid' do
      it 'subscribes to stream' do
        subscribe(id: friend.id)

        expect(subscription).to be_confirmed
        expect(subscription).to have_stream_for(friend)
      end
    end

    context 'as invalid' do
      it 'rejects subscription' do
        subscribe(id: -1)

        expect(subscription).to be_rejected
      end
    end
  end
end
