require 'rails_helper'

describe Api::V1::RegistrationsController, type: :controller do
  describe '#create' do
    subject { post(:create, format: :json, params: params) }

    let!(:params) do
      {
        telephone_number: 79000000000,
        username: 'tyler',
        name: 'Tyler The Compiler',
        password: 'secret'
      }
    end

    context 'happy path' do
      it 'returns JWT token' do
        expect(JWT).to receive(:encode)

        subject

        expect(response.status).to eq 200
        expect(response_data.keys).to contain_exactly(:token)
        response_status_should_be 'ok'
      end
    end

    context 'wrong_format' do
      before { params[:password] = '' }

      it 'returns wrong_format' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'wrong_format'
      end
    end

    context 'validation failed' do
      before { create(:user, number: 79000000000) }

      it 'returns validation_failed' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'validation_failed'
      end
    end
  end
end