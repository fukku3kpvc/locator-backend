require 'rails_helper'

describe Api::V1::TokensController, type: :controller do
  include_examples :session

  describe '#create' do
    subject { post(:create, params: params, format: :json) }

    let!(:params) do
      {
        token: 'sample',
        os: 'ios'
      }
    end

    context 'user with token' do
      let!(:token) { create(:push_token, user: user) }

      it 'updated current token' do
        expect { subject }.not_to change(PushToken, :count)
        expect(response.status).to eq 200
        response_status_should_be 'ok'
        expect(user.push_token.token).to eq('sample')
        expect(user.push_token.os).to eq('ios')
      end
    end

    context 'user without token' do
      it 'creates new token' do
        expect { subject }.to change(PushToken, :count).by(1)
        expect(response.status).to eq 200
        response_status_should_be 'ok'
        expect(user.push_token.token).to eq('sample')
        expect(user.push_token.os).to eq('ios')
      end
    end

    context 'invalid params' do
      context 'token' do
        before { params[:token] = '' }

        it 'returns wrong_format' do
          expect { subject }.not_to change(PushToken, :count)
          expect(response.status).to eq 200
          response_status_should_be 'wrong_format'
        end
      end

      context 'os' do
        before { params[:os] = 'invalid' }

        it 'returns wrong_format' do
          expect { subject }.not_to change(PushToken, :count)
          expect(response.status).to eq 200
          response_status_should_be 'wrong_format'
        end
      end
    end

    it_behaves_like :unauthorized
  end
end
