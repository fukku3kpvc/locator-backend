require 'rails_helper'

describe Api::V1::GeoZonesController, type: :controller do
  include_examples :session

  describe '#index' do
    let!(:target) { create(:user) }
    let!(:params) do
      {
        id: target.id
      }
    end

    subject { get(:index, params: params, format: :json) }

    context 'friendship exists' do
      let!(:friendship) do
        create(:friendship, :confirmed, initiator: user, recipient: target)
      end
      let!(:zone) do
        create(:geo_zone, friendship: friendship, target: target, watcher: user)
      end

      it 'lists geo zone' do
        subject

        expect(response_data.first[:zone_id]).to eq(zone.id)
        expect(response_data.first[:center]).not_to be_nil
        expect(response_data.first[:radius]).to eq(zone.radius)
        expect(response_data.first[:name]).to eq(zone.name)
        expect(response_data.first[:color]).to eq(zone.color)
        expect(response.status).to eq 200
        response_status_should_be 'ok'
      end

      context 'user is target' do
        let!(:zone) do
          create(:geo_zone, friendship: friendship, target: user, watcher: target)
        end

        it 'returns empty array' do
          subject

          expect(response_data).to be_empty
          expect(response.status).to eq 200
          response_status_should_be 'ok'
        end
      end

      context 'without geo zones' do
        before { GeoZone.destroy_all }

        it 'returns empty array' do
          subject

          expect(response_data).to be_empty
          expect(response.status).to eq 200
          response_status_should_be 'ok'
        end
      end
    end

    context 'non confirmed friendship' do
      let!(:friendship) do
        create(:friendship, :initiated, initiator: user, recipient: target)
      end

      it 'returns wrong_id' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'wrong_id'
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#create' do
    let!(:target) { create(:user) }
    let!(:params) do
      {
        id: target.id,
        radius: 100,
        name: 'Sample',
        color: '#eee',
        center: {
          longitude: 45.6854,
          latitude: 137.3357
        }
      }
    end

    subject { post(:create, params: params, format: :json) }

    context 'friendship exists' do
      let!(:point) do
        RGeo::Geographic.spherical_factory(srid: 4326)
                        .point(
                          params[:center][:longitude],
                          params[:center][:latitude]
                        )
      end

      context 'watcher is initiator' do
        let!(:friendship) do
          create(:friendship, :confirmed, initiator: user, recipient: target)
        end

        it 'creates geo zone' do
          subject

          expect(user.reload.watcher_geo_zones.size).to eq 1
          expect(target.reload.target_geo_zones.size).to eq 1
          expect(GeoZone.first.friendship).to eq friendship
          expect(GeoZone.first.watcher).to eq user
          expect(GeoZone.first.target).to eq target
          expect(GeoZone.first.center).to eq point
          expect(GeoZone.first.name).to eq params[:name]
          expect(GeoZone.first.radius).to eq params[:radius].to_i
          expect(GeoZone.first.color).to eq params[:color]
          expect(response.status).to eq 200
          response_status_should_be 'ok'
        end
      end

      context 'watcher is recipient' do
        let!(:friendship) do
          create(:friendship, :confirmed, initiator: target, recipient: user)
        end

        it 'creates geo zone' do
          subject

          expect(user.reload.watcher_geo_zones.size).to eq 1
          expect(target.reload.target_geo_zones.size).to eq 1
          expect(GeoZone.first.friendship).to eq friendship
          expect(GeoZone.first.watcher).to eq user
          expect(GeoZone.first.target).to eq target
          expect(GeoZone.first.center).to eq point
          expect(GeoZone.first.name).to eq params[:name]
          expect(GeoZone.first.radius).to eq params[:radius].to_i
          expect(GeoZone.first.color).to eq params[:color]
          expect(response.status).to eq 200
          response_status_should_be 'ok'
        end
      end

      context 'friendship is not confirmed' do
        let!(:friendship) do
          create(:friendship, :initiated, initiator: user, recipient: target)
        end

        it 'does not create geo zone and returns wrong_id' do
          subject

          expect(GeoZone.count).to be_zero
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
        end
      end
    end

    context 'target is not friend' do
      it 'does not create geo zone and returns wrong_id' do
        subject

        expect(GeoZone.count).to be_zero
        expect(response.status).to eq 200
        response_status_should_be 'wrong_id'
      end
    end

    context 'invalid params' do
      context 'User does not exist' do
        before { params[:id] = '1_000_000' }

        it 'does not create geo zone and returns wrong_id' do
          subject

          expect(GeoZone.count).to be_zero
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
        end
      end

      context 'radius' do
        context '< 100' do
          before { params[:radius] = 99 }

          it 'returns wrong_format' do
            subject

            expect(GeoZone.count).to be_zero
            expect(response.status).to eq 200
            response_status_should_be 'wrong_format'
          end
        end

        context '> 1_000' do
          before { params[:radius] = 1_001 }

          it 'returns wrong_format' do
            subject

            expect(GeoZone.count).to be_zero
            expect(response.status).to eq 200
            response_status_should_be 'wrong_format'
          end
        end
      end

      context 'name' do
        before { params[:name] = '' }

        it 'returns wrong_format' do
          subject

          expect(GeoZone.count).to be_zero
          expect(response.status).to eq 200
          response_status_should_be 'wrong_format'
        end
      end

      context 'color' do
        before { params[:color] = 'invalid' }

        it 'returns wrong_format' do
          subject

          expect(GeoZone.count).to be_zero
          expect(response.status).to eq 200
          response_status_should_be 'wrong_format'
        end
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#destroy' do
    let!(:target) { create(:user) }
    let!(:friendship) do
      create(:friendship, :confirmed, initiator: user, recipient: target)
    end
    let!(:zone) do
      create(:geo_zone, friendship: friendship, target: target, watcher: user)
    end
    let!(:params) do
      {
        id: target.id,
        zone_id: zone.id
      }
    end

    subject { delete(:destroy, params: params, format: :json) }

    it 'destroys zone' do
      expect { subject }.to change(GeoZone, :count).by(-1)
      expect(response.status).to eq 200
      response_status_should_be 'ok'
    end

    context 'user is target' do
      let!(:zone) do
        create(:geo_zone, friendship: friendship, target: user, watcher: target)
      end

      it 'does not destroy zone' do
        expect { subject }.not_to change(GeoZone, :count)
        expect(response.status).to eq 200
        response_status_should_be 'wrong_id'
      end
    end

    context 'zone does not exists' do
      before { GeoZone.destroy_all }

      it 'returns wrong_id' do
        expect { subject }.not_to change(GeoZone, :count)
        expect(response.status).to eq 200
        response_status_should_be 'wrong_id'
      end
    end

    it_behaves_like :unauthorized
  end
end
