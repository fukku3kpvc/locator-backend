require 'rails_helper'

describe Api::V1::SettingsController, type: :controller do
  include_examples :session

  describe '#show' do
    subject { get(:show, format: :json) }

    let!(:user) { create(:user, :with_avatar) }
    let!(:locations) { create(:location, user: user) }
    let!(:friend) { create(:user) }
    let!(:friendship) do
      create(:friendship, :confirmed, initiator: user, recipient: friend)
    end
    let!(:zone) do
      create(:geo_zone, watcher: user, target: friend, friendship: friendship)
    end
    let!(:data) do
      {
        username: user.username,
        name: user.full_name,
        telephone_number: user.number,
        avatar: user.serialize_avatar,
        friends_count: 1,
        geo_zones_count: 1,
        locations_count: 1,
        last_location: instance_of(Hash),
        app_version: '1.0 (27.06.2020)'
      }
    end
    let!(:last_location_data) do
      {
        longitude: locations.lon,
        latitude: locations.lat,
        address: locations.address,
        local_time: locations.updated_at.iso8601
      }
    end

    it 'shows user settings' do
      subject

      expect(response.status).to eq 200
      response_status_should_be 'ok'
      expect(response_data).to include(data)
      expect(response_data[:last_location]).to include(last_location_data)
    end
  end
end
