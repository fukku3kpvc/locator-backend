require 'rails_helper'

describe Api::V1::MetricsController, type: :controller do
  include_examples :session

  describe '#create' do
    let!(:params) do
      {
        battery: 57,
        datetime: '2020-03-18T17:11:25+05:00',
        location: {
          longitude: 45.6854,
          latitude: 137.3357
        }
      }
    end

    subject { post(:create, params: params, format: :json) }

    context 'user first location and metric', :default_gco_stub do
      let!(:point) do
        RGeo::Geographic.spherical_factory(srid: 4326)
                        .point(
                          params[:location][:longitude],
                          params[:location][:latitude]
                        )
      end
      let!(:job_params) do
        {
          user_id: user.id,
          longitude: point.lon,
          latitude: point.lat,
          address: 'Россия, Республика Башкортостан, Уфа, улица Карла Маркса, 12к6',
          local_time: params[:datetime]
        }
      end
      before do
        user.locations.destroy_all
        user.metrics.destroy_all
      end

      it 'saves new metric and location' do
        subject

        expect(user.metrics.size).to eq 1
        expect(user.metrics.first.battery).to eq params[:battery]
        expect(user.metrics.first.time_zone).to eq 'Asia/Yekaterinburg'
        expect(user.locations.size).to eq 1
        expect(response.status).to eq 200
        response_status_should_be 'ok'
      end

      it 'does not enqueue geo_event_job' do
        expect { subject }.not_to have_enqueued_job(GeoEventJob)
      end

      it 'enqueues location_notifier_job' do
        expect { subject }.to have_enqueued_job(LocationNotifierJob).with(job_params)
      end
    end

    context 'distance is less than 100 m', :default_gco_stub do
      let!(:location) { create(:location, user: user) }
      let!(:metric) { create(:metric, user: user) }

      before do
        params[:location][:longitude] = 54.725085
        params[:location][:latitude] = 55.940468
      end

      it 'it touches old location' do
        subject

        expect(user.locations.size).to eq 1
        expect(user.locations.first.created_at).not_to eq user.locations.first.updated_at
        expect(response.status).to eq 200
        response_status_should_be 'ok'
      end

      it 'does not enqueue geo_event_job' do
        expect { subject }.not_to have_enqueued_job(GeoEventJob)
      end

      it 'does not enqueue location_notifier_job' do
        expect { subject }.not_to have_enqueued_job(LocationNotifierJob)
      end
    end

    context 'distance is more than 100 m', :default_gco_stub do
      let!(:location) { create(:location, user: user) }
      let!(:metric) { create(:metric, user: user) }
      let!(:location_notifier_job_params) do
        {
          user_id: user.id,
          longitude: 54.724743,
          latitude: 55.943022,
          address: 'Россия, Республика Башкортостан, Уфа, улица Карла Маркса, 12к6',
          local_time: params[:datetime]
        }
      end
      let!(:current_point) do
        {
          lon: 54.724743,
          lat: 55.943022
        }
      end
      let!(:previous_point) do
        {
          lon: location.position.lon,
          lat: location.position.lat
        }
      end

      before do
        params[:location][:longitude] = 54.724743
        params[:location][:latitude] = 55.943022
      end

      it 'it creates new location' do
        subject

        expect(user.locations.size).to eq 2
        expect(response.status).to eq 200
        response_status_should_be 'ok'
      end

      it 'enqueues geo_event_job' do
        expect { subject }.to have_enqueued_job(GeoEventJob).with(
          user, current_point, previous_point
        )
      end

      it 'enqueues location_notifier_job' do
        expect { subject }.to have_enqueued_job(LocationNotifierJob).with(
          location_notifier_job_params
        )
      end
    end

    context 'battery level is same that last', :default_gco_stub do
      let!(:location) { create(:location, user: user) }
      let!(:metric) { create(:metric, user: user, battery: 57) }

      it 'it touches old metric' do
        subject

        expect(user.metrics.size).to eq 1
        expect(user.metrics.first.battery).to eq params[:battery]
        expect(user.metrics.first.created_at).not_to eq user.metrics.first.updated_at
        expect(response.status).to eq 200
        response_status_should_be 'ok'
      end
    end

    context 'battery level differs that last', :default_gco_stub do
      let!(:location) { create(:location, user: user) }
      let!(:metric) { create(:metric, user: user, battery: 100) }

      it 'it creates new metric' do
        subject

        expect(user.metrics.size).to eq 2
        expect(user.metrics.last.battery).to eq params[:battery]
        expect(response.status).to eq 200
        response_status_should_be 'ok'
      end
    end

    context 'battery input is invalid' do
      before { params[:battery] = 'invalid' }

      it 'it returns wrong_format' do
        subject

        expect(user.metrics.size).to eq 0
        expect(user.locations.size).to eq 0
        expect(response.status).to eq 200
        response_status_should_be 'wrong_format'
      end
    end

    context 'datetime input is invalid' do
      before { params[:datetime] = 'invalid' }

      it 'it returns wrong_format' do
        subject

        expect(user.metrics.size).to eq 0
        expect(user.locations.size).to eq 0
        expect(response.status).to eq 200
        response_status_should_be 'wrong_format'
      end
    end

    it_behaves_like :unauthorized
  end
end
