require 'rails_helper'

describe Api::V1::FriendsController, type: :controller do
  include_examples :session

  describe '#index' do
    subject { get(:index, format: :json) }

    context 'single friendship' do
      context 'as receiver' do
        let!(:initiator) { create(:user) }
        let!(:friendship) do
          create(:friendship, initiator: initiator, recipient: user)
        end
        let!(:output) do
          [
            {
              id: initiator.id,
              username: initiator.username,
              name: initiator.full_name,
              verified: false,
              allow_to_confirm: true
            }
          ]
        end

        it 'returns correct data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end

        context 'verified friendship' do
          before do
            friendship.confirm!
            output.first[:verified] = true
            output.first[:allow_to_confirm] = false
          end

          it 'returns correct data' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data).to eq output
          end
        end

        context 'rejected friendship' do
          before do
            friendship.reject!
            output.clear
          end

          it 'returns empty data' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data).to eq output
          end
        end
      end

      context 'as initiator' do
        let!(:recipient) { create(:user) }
        let!(:friendship) do
          create(:friendship, initiator: user, recipient: recipient)
        end
        let!(:output) do
          [
            {
              id: recipient.id,
              username: recipient.username,
              name: recipient.full_name,
              verified: false,
              allow_to_confirm: false
            }
          ]
        end

        it 'returns correct data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end

        context 'verified friendship' do
          before do
            friendship.confirm!
            output.first[:verified] = true
          end

          it 'returns correct data' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data).to eq output
          end
        end

        context 'rejected friendship' do
          before do
            friendship.reject!
            output.clear
          end

          it 'returns empty data' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data).to eq output
          end
        end
      end
    end

    context 'multiple friendships' do
      let!(:friend_1) { create(:user, :with_avatar) }
      let!(:friend_2) { create(:user) }
      let!(:friend_3) { create(:user) }
      let!(:friend_4) { create(:user) }
      let!(:friend_5) { create(:user) }

      let!(:initiated_friendship_1) do
        create(:friendship, :confirmed, initiator: user, recipient: friend_1)
      end
      let!(:initiated_friendship_2) do
        create(:friendship, :confirmed, initiator: user, recipient: friend_2)
      end
      let!(:initiated_friendship_3) do
        create(:friendship, :rejected, initiator: user, recipient: friend_3)
      end
      let!(:received_friendship_1) do
        create(:friendship, initiator: friend_4, recipient: user)
      end
      let!(:received_friendship_2) do
        create(:friendship, :rejected, initiator: friend_5, recipient: user)
      end

      let!(:represented_friendship_1) do
        ReceivedFriendshipRepresenter.new(received_friendship_1).to_hash.deep_symbolize_keys!
      end
      let!(:represented_friendship_2) do
        InitiatedFriendshipRepresenter.new(initiated_friendship_2).to_hash.deep_symbolize_keys!
      end
      let!(:represented_friendship_3) do
        InitiatedFriendshipRepresenter.new(initiated_friendship_1).to_hash.deep_symbolize_keys!
      end

      it 'returns correct data' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'ok'
        expect(response_data.size).to eq 3
        expect(response_data.first).to eq represented_friendship_1
        expect(response_data.second).to eq represented_friendship_2
        expect(response_data.third).to eq represented_friendship_3
      end

      context 'with location' do
        let!(:location) { create(:location, user: friend_1) }
        let(:represented_friendship_4) do
          {
            id: initiated_friendship_1.recipient.id,
            username: initiated_friendship_1.recipient.username,
            name: initiated_friendship_1.recipient.full_name,
            avatar: initiated_friendship_1.recipient.serialize_avatar,
            verified: initiated_friendship_1.confirmed?,
            allow_to_confirm: false,
            location: {
              longitude: initiated_friendship_1.recipient.locations.last.lon,
              latitude: initiated_friendship_1.recipient.locations.last.lat
            }
          }
        end

        it 'returns correct data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data.size).to eq 3
          expect(response_data.first).to eq represented_friendship_1
          expect(response_data.second).to eq represented_friendship_2
          expect(response_data.third).to eq represented_friendship_4
        end

        context 'received_friendship' do
          let!(:friend) { create(:user, :with_avatar) }
          let!(:location) { create(:location, user: friend) }
          let!(:received_friendship) do
            build(:friendship, :confirmed, initiator: friend, recipient: user)
          end
          let!(:represented_friendship) do
            {
              id: received_friendship.initiator.id,
              username: received_friendship.initiator.username,
              name: received_friendship.initiator.full_name,
              avatar: received_friendship.initiator.serialize_avatar,
              verified: received_friendship.confirmed?,
              allow_to_confirm: received_friendship.initiated?,
              location: {
                longitude: received_friendship.initiator.locations.last.lon,
                latitude: received_friendship.initiator.locations.last.lat
              }
            }
          end

          before do
            Friendship.destroy_all
            received_friendship.save!
          end

          it 'returns correct data' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data.size).to eq 1
            expect(response_data.first).to eq represented_friendship
          end
        end
      end
    end

    context 'without any friendship' do
      it 'returns empty data' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'ok'
        expect(response_data).to be_empty
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#find' do
    subject { get(:find, params: params, format: :json) }

    let!(:params) do
      {
        search_name: 'tyler'
      }
    end
    let!(:friend) { create(:user, username: 'tyler') }
    let!(:output) do
      [
        {
          id: friend.id,
          username: friend.username,
          name: friend.full_name
        }
      ]
    end

    context 'success' do
      context 'one field' do
        it 'founds it' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end

        context 'with avatar' do
          let!(:friend) { create(:user, :with_avatar, username: 'tyler') }

          it 'founds it and displays avatar' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data.first[:avatar]).not_to be_nil
          end
        end
      end

      context 'multiple fields' do
        let!(:friend_2) { create(:user, username: 'tyler_compiler') }
        let!(:output_part) do
          {
            id: friend_2.id,
            username: friend_2.username,
            name: friend_2.full_name
          }
        end

        before { output << output_part }

        it 'founds both friends' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end
      end

      context 'no fields' do
        let!(:output) { [] }

        before do
          friend.destroy!
          params[:search_name] = user.full_name
        end

        it 'returns empty data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end
      end
    end

    context 'errors' do
      context 'without search name' do
        before { params[:search_name] = '' }

        it 'returns wrong_format' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_format'
        end
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#show' do
    subject { get(:show, params: params, format: :json) }

    let!(:friend) { create(:user) }
    let!(:friendship) do
      create(:friendship, initiator: user, recipient: friend)
    end
    let!(:output) do
      {
        id: friend.id,
        telephone_number: friend.number,
        username: friend.username,
        name: friend.full_name
      }
    end
    let!(:params) do
      {
        id: friend.id
      }
    end

    context 'success' do
      context 'initiated friendship' do
        it 'returns success with correct output' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end
      end

      context 'received friendship' do
        let!(:friendship) do
          create(:friendship, initiator: friend, recipient: user)
        end

        it 'returns success with correct output' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output
        end
      end

      context 'friend with metrics and location' do
        let!(:metrics) { create(:metric, user: friend) }
        let!(:location) { create(:location, user: friend) }
        let!(:output_with_metrics) do
          {
            id: friend.id,
            telephone_number: friend.number,
            username: friend.username,
            name: friend.full_name,
            metrics: {
              battery: metrics.battery,
              local_time: metrics.local_time,
              location: {
                longitude: location.lon,
                latitude: location.lat,
                address: location.address
              }
            }
          }
        end

        it 'returns success with correct output' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to eq output_with_metrics
        end

        context 'received friendship' do
          let!(:friendship) do
            create(:friendship, initiator: friend, recipient: user)
          end

          it 'returns success with correct output' do
            subject

            expect(response.status).to eq 200
            response_status_should_be 'ok'
            expect(response_data).to eq output_with_metrics
          end
        end
      end
    end

    context 'failure' do
      context 'friend does not exist' do
        before { params[:id] = 123 }

        it 'returns wrong_id' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
        end
      end

      context 'rejected friendship' do
        let!(:friendship) do
          create(:friendship, :rejected, initiator: friend, recipient: user)
        end

        it 'returns wrong_id' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
        end
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#create' do
    subject { post(:create, params: params, format: :json) }

    let!(:friend) { create(:user) }
    let!(:params) do
      {
        id: friend.id
      }
    end

    context 'success' do
      it 'initiates a friendship' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'ok'
        expect(response_data).to be_nil
      end
    end

    context 'failure' do
      context 'friendship with the same person' do
        before { params[:id] = user.id }

        it 'returns wrong_id' do
          expect { subject }.not_to change(Friendship, :count)
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end

      context 'user does not exist' do
        before { friend.destroy }

        it 'returns wrong_id' do
          expect { subject }.not_to change(Friendship, :count)
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end

      context 'friend exists' do
        before do
          create(:friendship, initiator: friend, recipient: user)
        end

        it 'returns wrong_id' do
          expect { subject }.not_to change(Friendship, :count)
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end

      context 'invalid friendship' do
        before do
          allow_any_instance_of(Friendship).to receive(:valid?).and_return(false)
        end

        it 'returns wrong_id' do
          expect { subject }.not_to change(Friendship, :count)
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#verify' do
    subject { post(:verify, params: params, format: :json) }

    let!(:friend) { create(:user) }
    let!(:friendship) do
      create(:friendship, :initiated, initiator: friend, recipient: user)
    end
    let!(:params) do
      {
        id: friend.id,
        verified: true
      }
    end

    context 'success' do
      context 'confirm' do
        it 'confirms friendship' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to be_nil
          expect(friendship.reload.available_at).not_to be_nil
          expect(friendship.reload.confirmed?).to be true
        end
      end

      context 'reject' do
        before { params[:verified] = false }

        it 'rejects friendship' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to be_nil
          expect(friendship.reload.available_at).to be_nil
          expect(friendship.reload.rejected?).to be true
        end
      end
    end

    context 'failure' do
      context 'friendship does not exist' do
        before { Friendship.destroy_all }

        it 'returns wrong_id' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end

      context 'friendship is not initiated' do
        before { friendship.reject! }

        it 'returns wrong_id' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end

      context 'user is not recipient' do
        before do
          friendship.update(recipient: friend, initiator: user)
        end

        it 'returns wrong_id' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end
    end

    it_behaves_like :unauthorized
  end

  describe '#destroy' do
    subject { delete(:destroy, params: params, format: :json) }

    let!(:friend) { create(:user) }
    let!(:params) do
      {
        id: friend.id,
        verified: true
      }
    end

    context 'success' do
      context 'user is initiator' do
        let!(:friendship) do
          create(:friendship, :confirmed, initiator: user, recipient: friend)
        end

        it 'destroys friendship' do
          expect { subject }.to change(Friendship, :count).by(-1)
          expect { friendship.reload }.to raise_error ActiveRecord::RecordNotFound
          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to be_nil
        end
      end

      context 'user is recipient' do
        let!(:friendship) do
          create(:friendship, :confirmed, initiator: friend, recipient: user)
        end

        it 'destroys friendship' do
          expect { subject }.to change(Friendship, :count).by(-1)
          expect { friendship.reload }.to raise_error ActiveRecord::RecordNotFound
          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to be_nil
        end
      end
    end

    context 'failure' do
      context 'user is not friend' do
        it 'returns wrong_id' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end

      context 'friendship is not confirmed' do
        let!(:friendship) do
          create(:friendship, :initiated, initiator: friend, recipient: user)
        end

        it 'returns wrong_id' do
          expect { subject }.not_to change(Friendship, :count)
          expect(friendship.reload.initiated?).to be true
          expect(response.status).to eq 200
          response_status_should_be 'wrong_id'
          expect(response_data).to be_nil
        end
      end
    end

    it_behaves_like :unauthorized
  end
end