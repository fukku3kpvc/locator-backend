require 'rails_helper'

describe Api::V1::TracksController, type: :controller do
  include_examples :session

  describe '#index' do
    subject { get(:index, params: params, format: :json) }

    let!(:time) { Time.current }
    let!(:friend) { create(:user) }
    let!(:location_1) do
      create(:location, user: friend, updated_at: time - 1.day)
    end
    let!(:location_2) do
      create(:location, user: friend, updated_at: time)
    end
    let!(:location_3) do
      create(:location, user: friend, updated_at: time + 1.day)
    end
    let!(:location_4) do
      create(:location, user: friend, updated_at: time + 2.days)
    end
    let!(:params) do
      {
        id: friend.id,
        from: time - 1.day,
        to: time + 1.day
      }
    end

    context 'friendship exists' do
      let!(:friendship) do
        create(:friendship, :confirmed, initiator: user, recipient: friend)
      end

      it 'lists all points' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'ok'
        expect(response_data.size).to eq(3)
        expect(response_data.first[:timestamp]).to eq(location_1.updated_at.iso8601)
        expect(response_data.second[:timestamp]).to eq(location_2.updated_at.iso8601)
        expect(response_data.last[:timestamp]).to eq(location_3.updated_at.iso8601)
      end

      context 'locations does not exist' do
        before { Location.destroy_all }

        it 'lists empty data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'ok'
          expect(response_data).to be_empty
        end
      end
    end

    context 'friendship does not exist' do
      it 'returns wrong_id' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'wrong_id'
      end
    end

    it_behaves_like :unauthorized
  end
end
