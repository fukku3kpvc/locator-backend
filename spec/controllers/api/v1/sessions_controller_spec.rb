require 'rails_helper'

describe Api::V1::SessionsController, type: :controller do
  describe '#create' do
    subject { post(:create, format: :json, params: params) }

    let!(:user) do
      create(:user, number: 79999999999, username: 'tester', password: 'secret')
    end
    let!(:params) do
      {
        login: 79999999999,
        password: 'secret'
      }
    end

    context 'happy path' do
      context 'login is number' do
        it 'returns JWT token' do
          subject

          expect(response.status).to eq 200
          expect(response_data.keys).to contain_exactly(:token)
          response_status_should_be 'ok'
        end
      end

      context 'login is username' do
        before { params[:login] = 'tester' }

        it 'returns JWT token' do
          subject

          expect(response.status).to eq 200
          expect(response_data.keys).to contain_exactly(:token)
          response_status_should_be 'ok'
        end
      end
    end

    context 'wrong_data' do
      context 'when user does not exist' do
        before { User.destroy_all }

        it 'returns wrong_data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_data'
        end
      end

      context 'when password is incorrect' do
        before { params[:password] = '' }

        it 'returns wrong_data' do
          subject

          expect(response.status).to eq 200
          response_status_should_be 'wrong_data'
        end
      end
    end
  end
end