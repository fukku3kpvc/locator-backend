require 'rails_helper'

describe Friendship, type: :model do
  let!(:initiator) { create(:user) }
  let!(:recipient) { create(:user) }

  describe 'associations' do
    subject { described_class.create(initiator: initiator, recipient: recipient) }

    it 'creates model instance' do
      expect { subject }.to change { described_class.count }.from(0).to(1)
      expect(described_class.first.initiator).to eq(initiator)
      expect(described_class.first.recipient).to eq(recipient)
    end
  end

  describe 'validations' do
    subject { described_class.create(initiator: initiator, recipient: recipient) }

    context 'when friendship exists' do
      before { create(:friendship, initiator: initiator, recipient: recipient) }

      it 'does not create another instance' do
        expect { subject }.not_to change { described_class.count }
      end

      context 'when members swaps' do
        subject { described_class.create(initiator: recipient, recipient: initiator) }

        it 'does not create another instance' do
          expect { subject }.not_to change { described_class.count }
        end
      end
    end
  end

  describe 'state' do
    let!(:friendship) { create(:friendship) }

    context 'confirm' do
      subject(:process) { friendship.confirm! }

      it 'processes the event' do
        expect { process }.to change { friendship.state }.from('initiated')
                                                         .to('confirmed')
      end

      context 'when friendship is not initiated' do
        let!(:friendship) { create(:friendship, :rejected) }

        it 'raises an error' do
          expect { process }.to raise_error(AASM::InvalidTransition)
        end
      end
    end

    context 'reject' do
      subject(:process) { friendship.reject! }

      it 'processes the event' do
        expect { process }.to change { friendship.state }.from('initiated')
                                                         .to('rejected')
      end

      context 'when friendship is not initiated' do
        let!(:friendship) { create(:friendship, :confirmed) }

        it 'raises an error' do
          expect { process }.to raise_error(AASM::InvalidTransition)
        end
      end
    end
  end
end
