require 'rails_helper'

describe Metric, type: :model do
  it { should belong_to(:user) }

  describe 'association' do
    let!(:user) { create(:user) }
    let!(:metric) { create(:metric, user: user) }

    subject(:destroy_user) { user.destroy }

    it 'also destroys associated location' do
      expect { destroy_user }.to change(described_class, :count).by(-1)
    end
  end

  describe 'validation' do
    context 'battery' do
      let!(:metric) { build(:metric, battery: -10) }

      subject(:save_metrics) { metric.save! }

      it 'does not save metric with invalid battery' do
        expect { save_metrics }.to raise_error(ActiveRecord::RecordInvalid)
      end

      context 'greater_than' do
        let!(:metric) { build(:metric, battery: 1) }

        it 'does not save metric with invalid battery' do
          expect { save_metrics }.to change { Metric.count }.by(1)
        end
      end

      context 'less_than' do
        let!(:metric) { build(:metric, battery: 100) }

        it 'does not save metric with invalid battery' do
          expect { save_metrics }.to change { Metric.count }.by(1)
        end
      end
    end
  end
end
