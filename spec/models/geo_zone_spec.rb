require 'rails_helper'

describe GeoZone, type: :model do
  let!(:creator) { create(:user) }
  let!(:friend) { create(:user) }
  let!(:friendship) do
    create(:friendship, :confirmed, initiator: creator, recipient: friend)
  end

  describe 'association' do
    let!(:geo_zone) do
      create(:geo_zone, friendship: friendship, watcher: creator, target: friend)
    end

    subject(:user_destroy) { creator.destroy }
    subject(:friendship_destroy) { friendship.destroy }

    it 'destroys on user destroy' do
      expect { user_destroy }.to change(described_class, :count).by(-1)
    end

    it 'destroys on friendship destroy' do
      expect { friendship_destroy }.to change(described_class, :count).by(-1)
    end
  end

  describe 'validations' do
    subject(:save_model) { geo_zone.save }

    context 'radius' do
      let!(:geo_zone) do
        build(:geo_zone, friendship: friendship, watcher: creator, target: friend, radius: 0)
      end

      it 'does not save model with wrong radius value' do
        expect { save_model }.not_to change(described_class, :count)
      end
    end

    context 'users' do
      let!(:another_user) { create(:user) }

      let!(:geo_zone) do
        build(:geo_zone, friendship: friendship, watcher: another_user, target: friend)
      end

      it 'does not save model with user that is not friendship participant' do
        expect { save_model }.not_to change(described_class, :count)
      end
    end

    context 'empty fields' do
      let!(:geo_zone) do
        build(:geo_zone, friendship: friendship, watcher: creator, target: friend, radius: nil)
      end

      it 'does not save model with empty fields' do
        expect { save_model }.not_to change(described_class, :count)
      end
    end
  end

  describe '.containing' do
    subject { described_class.containing(point.lon, point.lat) }

    let!(:center) do
      RGeo::Geographic.spherical_factory(srid: 4326)
                      .point(55.993222, 54.743822)
    end
    let!(:zone) do
      create(
        :geo_zone, center: center, radius: 100, watcher: creator,
                   target: friend, friendship: friendship
      )
    end

    context 'one zone' do
      context 'containing' do
        let!(:point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.99394, 54.743553)
        end

        it 'returns one zone' do
          expect(subject.size).to eq(1)
          expect(subject.first).to eq(zone)
        end
      end

      context 'not containing' do
        let!(:point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.994946, 54.743224)
        end

        it 'returns empty relation' do
          expect(subject.size).to eq(0)
        end
      end
    end

    context 'multiple zones' do
      let!(:center_2) do
        RGeo::Geographic.spherical_factory(srid: 4326)
                        .point(55.993988, 54.743539)
      end
      let!(:zone_2) do
        create(
          :geo_zone, center: center, radius: 200, watcher: creator,
                     target: friend, friendship: friendship
        )
      end

      context 'containing all' do
        let!(:point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.993655, 54.743735)
        end

        it 'returns all zones' do
          expect(subject.size).to eq(2)
          expect(subject.first).to eq(zone)
          expect(subject.last).to eq(zone_2)
        end
      end

      context 'containing partial' do
        let!(:point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.994904, 54.743274)
        end

        it 'returns one zone' do
          expect(subject.size).to eq(1)
          expect(subject.first).to eq(zone_2)
        end
      end

      context 'not containing' do
        let!(:point) do
          RGeo::Geographic.spherical_factory(srid: 4326)
                          .point(55.996804, 54.742249)
        end

        it 'returns empty relation' do
          expect(subject.size).to eq(0)
        end
      end
    end
  end
end
