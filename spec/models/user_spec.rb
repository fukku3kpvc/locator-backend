require 'rails_helper'

describe User, type: :model do
  it { should have_many(:locations).dependent(:destroy) }
  it { should have_many(:metrics).dependent(:destroy) }

  describe 'validations' do
    context 'uniqueness' do
      subject(:not_unique_username) do
        described_class.new(
          username: 'uniq',
          number: 70000000000,
          full_name: 'sample',
          password: 'sample'
        )
      end
      subject(:not_unique_number) do
        described_class.new(
          username: 'sample',
          number: 79999999999,
          full_name: 'sample',
          password: 'sample'
        )
      end

      before { create(:user, username: 'uniq', number: 79999999999) }

      it 'has unique username' do
        expect(not_unique_username).to be_invalid
      end

      it 'has unique number' do
        expect(not_unique_number).to be_invalid
      end
    end

    context 'identical_participants' do
      let!(:user) { create(:user) }

      subject { build(:friendship, recipient: user, initiator: user) }

      it 'does not create friendship' do
        expect { subject.save }.not_to change(Friendship, :count)
      end
    end
  end

  describe 'callbacks' do
    context 'with friendship' do
      let!(:me) { create(:user) }
      let!(:friend) { create(:user) }
      let!(:friend_2) { create(:user) }
      let!(:friendship) { create(:friendship, recipient: friend, initiator: me) }
      let!(:friendship_2) { create(:friendship, recipient: me, initiator: friend_2) }

      subject { me.destroy }

      it 'destroys all friendships on user destroy' do
        expect { subject }.to change(Friendship, :count).by(-2)
        expect { friendship.reload }.to raise_error ActiveRecord::RecordNotFound
        expect { friendship_2.reload }.to raise_error ActiveRecord::RecordNotFound
        expect { me.reload }.to raise_error ActiveRecord::RecordNotFound
        expect(Friendship.count).to eq 0
        expect(described_class.count).to eq 2
      end
    end
  end

  describe '#create' do
    context 'with avatar' do
      subject { create(:user, :with_avatar) }

      it 'creates user' do
        expect { subject }.to change { described_class.count }.by(1)
        expect(described_class.first)
      end

      context 'representer' do
        let!(:user) { create(:user, :with_avatar) }
        subject { UserSearchRepresenter.new(user).to_hash }

        it 'displays user avatar' do
          expect(subject['avatar']).not_to be_nil
        end
      end
    end
  end
end
