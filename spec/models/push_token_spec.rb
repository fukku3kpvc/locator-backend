require 'rails_helper'

describe PushToken, type: :model do
  it { should belong_to(:user) }

  describe 'association' do
    let!(:user) { create(:user) }
    let!(:token) { create(:push_token, user: user) }

    subject(:destroy_user) { user.destroy }

    it 'also destroys associated location' do
      expect { destroy_user }.to change(described_class, :count).by(-1)
    end
  end

  describe 'validation' do
    subject(:save_token) { token.save! }

    context 'os' do
      let!(:token) { build(:push_token, os: 'invalid') }

      it 'does not save token with invalid os' do
        expect { save_token }.to raise_error(ActiveRecord::StatementInvalid)
      end

      context 'valid_os' do
        context 'ios' do
          let!(:token) { build(:push_token, os: 'ios') }

          it 'saves token' do
            expect { save_token }.to change { described_class.count }.by(1)
            expect(described_class.last.os).to eq('ios')
          end
        end

        context 'android' do
          let!(:token) { build(:push_token, os: 'android') }

          it 'saves token' do
            expect { save_token }.to change { described_class.count }.by(1)
            expect(described_class.last.os).to eq('android')
          end
        end
      end
    end

    context 'token' do
      context 'valid token' do
        let!(:token) { build(:push_token) }

        it 'saves token' do
          expect { save_token }.to change { described_class.count }.by(1)
          expect(described_class.last.token).not_to be_nil
        end
      end

      context 'empty token' do
        let!(:token) { build(:push_token, token: '') }

        it 'does not save token' do
          expect { save_token }.to raise_error(ActiveRecord::RecordInvalid)
        end
      end
    end
  end
end
