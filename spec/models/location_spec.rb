require 'rails_helper'

describe Location, type: :model do
  it { should belong_to(:user) }

  describe 'association' do
    let!(:user) { create(:user) }
    let!(:location) { create(:location, user: user) }

    subject(:destroy_user) { user.destroy }

    it 'also destroys associated location' do
      expect { destroy_user }.to change(described_class, :count).by(-1)
    end
  end
end
