require 'rails_helper'

describe Clients::ExpoPush::Client do
  describe '#send_push' do
    subject { described_class.new.send_push(to, title, body, ttl, sound) }

    let!(:to) { 'sample_token' }
    let!(:title) { 'hello' }
    let!(:body) { 'world' }
    let!(:ttl) { nil }
    let!(:sound) { nil }
    let!(:payload) do
      [
        {
          to: 'ExponentPushToken[sample_token]',
          title: 'hello',
          body: 'world',
          ttl: 300,
          sound: 'default'
        }
      ].to_json
    end

    before do
      stub_request(:post, 'https://exp.host/--/api/v2/push/send').with(
        body: payload
      )
    end

    context 'success' do
      it 'sends push' do
        expect(subject).to eq('ok')
      end
    end

    context 'invalid token' do
      before do
        allow_any_instance_of(
          Exponent::Push::ResponseHandler
        ).to receive(:errors?).and_return(true)

        allow_any_instance_of(
          Exponent::Push::ResponseHandler
        ).to receive(:errors).and_return([Exponent::Push::DeviceNotRegisteredError])
      end

      it 'raises error from expo response' do
        expect { subject }.to raise_error(Exponent::Push::DeviceNotRegisteredError)
      end
    end
  end
end
