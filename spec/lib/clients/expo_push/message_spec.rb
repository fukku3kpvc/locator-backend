require 'rails_helper'

describe Clients::ExpoPush::Message do
  describe '#build' do
    subject { described_class.new(to, title, body, ttl, sound).build }

    let!(:to) { 'sample_token' }
    let!(:title) { 'hello' }
    let!(:body) { 'world' }
    let!(:ttl) { nil }
    let!(:sound) { nil }

    let!(:message) do
      [
        {
          to: "ExponentPushToken[#{to}]",
          title: 'hello',
          body: 'world',
          ttl: 300,
          sound: 'default'
        }
      ]
    end

    it 'builds correct message' do
      expect(subject).to eq(message)
    end
  end
end
