require 'rails_helper'

describe LocationNotifierJob, type: :job do
  describe '#perform_later' do
    let!(:user) { create(:user) }
    let!(:point) do
      RGeo::Geographic.spherical_factory(srid: 4326)
                      .point(54.8376, 56.0613)
    end
    let!(:location) do
      {
        user_id: user.id,
        longitude: point.lon,
        latitude: point.lat,
        address: 'улица Пушкина, дом Калатушкина',
        local_time: Time.zone.now.iso8601
      }
    end

    subject do
      perform_enqueued_jobs do
        described_class.perform_later(location)
      end
    end

    it "broadcasts location to user's channel" do
      expect { subject }.to have_broadcasted_to(user).from_channel(LocationsChannel).with(location)
    end
  end
end
