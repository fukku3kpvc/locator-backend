require 'rails_helper'

describe GeoEventJob, type: :job do
  describe '#perform_later' do
    subject do
      perform_enqueued_jobs do
        described_class.perform_later(target, current_point, previous_point)
      end
    end

    let!(:target) { create(:user) }
    let!(:watcher) { create(:user) }
    let!(:token) { create(:push_token, user: watcher) }
    let!(:friendship) do
      create(:friendship, :confirmed, initiator: watcher, recipient: target)
    end
    let!(:center) do
      RGeo::Geographic.spherical_factory(srid: 4326)
                      .point(55.993222, 54.743822)
    end
    let!(:zone) do
      create(
        :geo_zone, center: center, radius: 100, watcher: watcher,
                   target: target, friendship: friendship
      )
    end

    context 'entered zone' do
      let!(:current_point) do
        {
          lon: 55.99394,
          lat: 54.743553
        }
      end
      let!(:previous_point) do
        {
          lon: 55.992487,
          lat: 54.740456
        }
      end
      let!(:body) { "Пользователь #{target.full_name} вошёл в зону #{zone.name}!" }

      it 'builds entered zone event and notifies watcher' do
        expect_any_instance_of(Tokens::Notify).to receive(:call).with(
          token: token.token, title: 'Новое событие', body: body
        ).once

        subject
      end

      context 'user without push token' do
        before { PushToken.destroy_all }

        it 'NOT notifies watcher' do
          expect_any_instance_of(Tokens::Notify).not_to receive(:call)

          subject
        end
      end
    end

    context 'left zone' do
      let!(:current_point) do
        {
          lon: 55.992487,
          lat: 54.740456
        }
      end
      let!(:previous_point) do
        {
          lon: 55.99394,
          lat: 54.743553
        }
      end
      let!(:body) { "Пользователь #{target.full_name} вышел из зоны #{zone.name}!" }

      it 'builds entered zone event and notifies watcher' do
        expect_any_instance_of(Tokens::Notify).to receive(:call).with(
          token: token.token, title: 'Новое событие', body: body
        ).once

        subject
      end
    end

    context 'both events' do
      let!(:center_2) do
        RGeo::Geographic.spherical_factory(srid: 4326)
                        .point(55.97978, 54.737067)
      end
      let!(:zone_2) do
        create(
          :geo_zone, center: center_2, radius: 100, watcher: watcher,
                     target: target, friendship: friendship, name: 'отчуждения'
        )
      end
      let!(:current_point) do
        {
          lon: 55.97978,
          lat: 54.737067
        }
      end
      let!(:previous_point) do
        {
          lon: 55.993222,
          lat: 54.743822
        }
      end
      let!(:left_body) { "Пользователь #{target.full_name} вышел из зоны #{zone.name}!" }
      let!(:entered_body) { "Пользователь #{target.full_name} вошёл в зону #{zone_2.name}!" }
      let!(:notify) { double(Tokens::Notify) }

      before do
        allow(Tokens::Notify).to receive(:new).and_return(notify)
      end

      it 'builds both events and notifies watcher' do
        expect(notify).to receive(:call).with(
          token: token.token, title: 'Новое событие', body: entered_body
        ).once

        expect(notify).to receive(:call).with(
          token: token.token, title: 'Новое событие', body: left_body
        ).once

        subject
      end
    end
  end
end
