RSpec.shared_examples :returns_error_hash do |error_name|
  it "returns failure result with error #{error_name} hash" do
    expect(subject).to be_failure
    expect(subject.failure).to be_instance_of(Hash)
    expect(subject.failure[:result]).to eq(error_name)
  end
end