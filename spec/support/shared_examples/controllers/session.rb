shared_examples :session do
  let!(:user) { create(:user) }

  before do
    allow_any_instance_of(described_class).to receive(:current_user).and_return(user)
  end
end

shared_examples :unauthorized do
  describe 'unauthorized request' do
    before do
      allow_any_instance_of(described_class).to receive(:current_user).and_call_original
    end

    context 'request without token' do
      it 'returns wrong_session_token' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'wrong_session_token'
      end
    end

    context 'request with incorrect token' do
      before { request.headers.merge!({'SESSION-TOKEN' => 'rand_token'}) }

      it 'returns wrong_session_token' do
        subject

        expect(response.status).to eq 200
        response_status_should_be 'wrong_session_token'
      end
    end
  end
end
