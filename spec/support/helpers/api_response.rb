def response_data
  data = JSON.parse(response.body)['data']
  data.deep_symbolize_keys! if data.is_a?(Hash)
  data = data.map(&:deep_symbolize_keys!) if data.is_a?(Array)
  data
end

def response_status_should_be(status)
  response_status = JSON.parse(response.body)['result']
  expect(response_status.to_s).to eq status
end

def response_result_should_be(result)
  response_result = JSON.parse(response.body)['data']
  result = (result == :empty ? nil : result).to_json
  expect(response_result).to eq JSON.parse(result)
end