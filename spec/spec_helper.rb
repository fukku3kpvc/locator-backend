RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.filter_run :focus
  config.run_all_when_everything_filtered = true
  config.order = :random
  Kernel.srand config.seed

  config.before(:all) do
    SimpleCov.start
  end

  config.after(:all) do
    FileUtils.rm_rf(File.join(Rails.root, 'public', 'uploads'))
  end

  config.before(:each) do
    Redis.new(Settings.redis.to_hash).flushdb
  end

  config.after(:each) do
    Timecop.return
  end
end
