source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.1'

# -- Core -- #
gem 'config' # Effortless multi-environment settings in Rails, Sinatra, Pandrino and others (https://github.com/railsconfig/config)
gem 'puma', '~> 3.11' # Puma is a simple, fast, threaded, and highly concurrent HTTP 1.1 server for Ruby/Rack applications (http://puma.io)
gem 'rails', '~> 5.2.4', '>= 5.2.4.1' # Full-stack web application framework. (http://rubyonrails.org)
gem 'rest-client' # Simple HTTP and REST client for Ruby, inspired by microframework syntax for specifying actions. (https://github.com/rest-client/rest-client)

# -- Infrastructure & Storage -- #
gem 'activerecord-postgis-adapter' # ActiveRecord adapter for PostGIS, based on RGeo. (http://github.com/rgeo/activerecord-postgis-adapter)
gem 'clockwork' # A scheduler process to replace cron. (http://github.com/Rykian/clockwork)
gem 'pg', '>= 0.18', '< 2.0' # Pg is the Ruby interface to the {PostgreSQL RDBMS}[http://www.postgresql.org/] (https://github.com/ged/ruby-pg)
gem 'redis' # A Ruby client library for Redis (https://github.com/redis/redis-rb)
gem 'redis-rails' # Redis for Ruby on Rails (http://redis-store.org/redis-rails)
gem 'rgeo' # RGeo is a geospatial data library for Ruby. (https://github.com/rgeo/rgeo)

# -- Authentication -- #
gem 'bcrypt' # OpenBSD's bcrypt() password hashing algorithm. (https://github.com/codahale/bcrypt-ruby)
gem 'jwt' # JSON Web Token implementation in Ruby (https://github.com/jwt/ruby-jwt)

# -- Attachments & Extensions -- #
gem 'carrierwave' # Ruby file upload library (https://github.com/carrierwaveuploader/carrierwave)
gem 'carrierwave-base64' # Upload images encoded as base64 to carrierwave. (https://github.com/lebedev-yury/carrierwave-base64)
gem 'exponent-server-sdk' # Exponent Server SDK
gem 'mini_magick' # Manipulate images with minimal use of memory via ImageMagick / GraphicsMagick (https://github.com/minimagick/minimagick)
gem 'shrine' # Toolkit for file attachments in Ruby applications (https://shrinerb.com)
gem 'shrine-memory' # Provides in-memory storage for Shrine. (https://github.com/janko-m/shrine-memory)
gem 'shrine-webdav' # Provides a simple WebDAV storage for Shrine. (https://github.com/funbox/shrine-webdav)

# -- Architecture Patterns -- #
gem 'aasm', '~> 4.12' # State machine mixin for Ruby objects (https://github.com/aasm/aasm)
gem 'draper', '~> 3.1.0' # View Models for Rails (http://github.com/drapergem/draper)
gem 'dry-auto_inject' # Container-agnostic automatic constructor injection (https://github.com/dry-rb/dry-auto_inject)
gem 'dry-container' # A simple container intended for use as an IoC container (https://github.com/dry-rb/dry-container)
gem 'dry-transaction', '~> 0.13.0' # Business Transaction Flow DSL (https://github.com/dry-rb/dry-transaction)
gem 'dry-validation', '~> 0.11' # A simple validation library (https://github.com/dry-rb/dry-validation)
gem 'representable' # Renders and parses JSON/XML/YAML documents from and to Ruby objects. Includes plain properties, collections, nesting, coercion and more. (https://github.com/trailblazer/representable/)

# -- Helpers -- #
gem 'geocoder' # Complete geocoding solution for Ruby. (http://www.rubygeocoder.com)
gem 'pg_search' # PgSearch builds Active Record named scopes that take advantage of PostgreSQL's full text search (https://github.com/Casecommons/pg_search)
gem 'savon' # Heavy metal SOAP client (http://savonrb.com)

# -- Deployment -- #
gem 'capistrano', '~> 3.11' # Capistrano - Welcome to easy deployment with Ruby over SSH (https://capistranorb.com/)
gem 'capistrano-passenger', '~> 0.2.0' # Passenger support for Capistrano 3.x (https://github.com/capistrano/passenger)
gem 'capistrano-rails', '~> 1.4' # Rails specific Capistrano tasks (https://github.com/capistrano/rails)
gem 'capistrano-rbenv', '~> 2.1', '>= 2.1.4' # rbenv integration for Capistrano (https://github.com/capistrano/rbenv)

group :development, :test do
  # -- Console and Debugging -- #
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw] # Ruby fast debugger - base + CLI (https://github.com/deivid-rodriguez/byebug)
  gem 'pry-byebug', platform: :mri # Fast debugging with Pry. (https://github.com/deivid-rodriguez/pry-byebug)
  gem 'pry-rails' # Use Pry as your rails console (https://github.com/rweng/pry-rails)

  # -- Tests -- #
  gem 'action-cable-testing' # Testing utils for Action Cable (http://github.com/palkan/action-cable-testing)
  gem 'bullet' # help to kill N+1 queries and unused eager loading. (https://github.com/flyerhzm/bullet)
  gem 'factory_bot_rails' # factory_bot_rails provides integration between factory_bot and rails 4.2 or newer (https://github.com/thoughtbot/factory_bot_rails)
  gem 'ffaker' # Ffaker generates dummy data. (https://github.com/ffaker/ffaker)
  gem 'fitting', '~> 2.2' # Validation in the rspec of API Blueprint (https://github.com/funbox/fitting)
  gem 'rails-controller-testing' # Extracting `assigns` and `assert_template` from ActionDispatch. (https://github.com/rails/rails-controller-testing)
  gem 'rspec' # rspec-3.9.0 (http://github.com/rspec)
  gem 'rspec-rails' # RSpec for Rails (https://github.com/rspec/rspec-rails)
  gem 'shoulda-matchers', '~> 4.0' # Simple one-liner tests for common Rails functionality (https://matchers.shoulda.io/)

  # -- Annotations -- #
  gem 'annotate' # Annotates Rails Models, routes, fixtures, and others based on the database schema. (http://github.com/ctran/annotate_models)
  gem 'annotate_gem' # Add comments to your Gemfile with each dependency's description. (https://github.com/ivantsepp/annotate_gem)
end

group :development do
  # -- Development Supports -- #
  gem 'listen', '>= 3.0.5', '< 3.2' # Listen to file modifications (https://github.com/guard/listen)
  gem 'spring' # Rails application preloader (https://github.com/rails/spring)
  gem 'spring-watcher-listen', '~> 2.0.0' # Makes spring watch files using the listen gem. (https://github.com/jonleighton/spring-watcher-listen)
  gem 'web-console', '>= 3.3.0' # A debugging tool for your Ruby on Rails applications. (https://github.com/rails/web-console)
end

group :test do
  # -- Test Supports -- #
  gem 'fakeredis' # Fake (In-memory) driver for redis-rb. (https://guilleiguaran.github.com/fakeredis)
  gem 'simplecov' # Code coverage for Ruby 1.9+ with a powerful configuration library and automatic merging of coverage across test suites (http://github.com/colszowka/simplecov)
  gem 'test-prof' # Ruby applications tests profiling tools (http://github.com/palkan/test-prof)
  gem 'timecop' # A gem providing "time travel" and "time freezing" capabilities, making it dead simple to test time-dependent code.  It provides a unified method to mock Time.now, Date.today, and DateTime.now in a single call. (https://github.com/travisjeffery/timecop)
  gem 'webmock' # Library for stubbing HTTP requests in Ruby. (http://github.com/bblimke/webmock)
end

# -- Additional -- #
gem 'bootsnap', '>= 1.1.0', require: false # Boot large ruby/rails apps faster (https://github.com/Shopify/bootsnap)
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby] # Timezone Data for TZInfo (http://tzinfo.github.io)
