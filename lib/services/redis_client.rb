module Services
  class RedisClient
    include Singleton

    attr_reader :redis

    def self.redis
      instance.redis
    end

    def initialize
      @redis ||= Redis.new(Settings.redis.to_hash)
    end
  end
end