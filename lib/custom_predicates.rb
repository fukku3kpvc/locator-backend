module CustomPredicates
  include Dry::Logic::Predicates

  predicate(:integer_convertible?) do |value|
    (value.to_i.to_s == value) || (value.is_a? Integer)
  end

  predicate(:coordinates?) do |value|
    /[-.\d]+/.match?(value.to_s)
  end

  predicate(:bool_convertible?) do |value|
    %w[true false].include?(value.downcase)
  end

  predicate(:hex?) do |value|
    /[#]\S+/.match?(value.to_s)
  end

  predicate(:point?) do |value|
    value.class == RGeo::Geographic::SphericalPointImpl
  end
end
