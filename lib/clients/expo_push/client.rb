module Clients
  module ExpoPush
    class Client
      attr_reader :client

      def initialize
        @client = Exponent::Push::Client.new
      end

      def send_push(to, title, body, ttl = nil, sound = nil)
        RL.info("Notifying #{to}, title: #{title}, body: #{body}")
        message = Message.new(to, title, body, ttl, sound)
        handler = client.send_messages(message.build)

        raise handler.errors.first if handler.errors?

        'ok'
      end
    end
  end
end
