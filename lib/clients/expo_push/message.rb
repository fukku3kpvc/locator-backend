module Clients
  module ExpoPush
    class Message
      attr_reader :to, :title, :body, :ttl, :sound

      def initialize(to, title, body, ttl = nil, sound = nil)
        @to = "ExponentPushToken[#{to}]"
        @title = title
        @body = body
        @ttl = ttl.present? ? ttl : Settings.expo.push.ttl
        @sound = sound.present? ? sound : Settings.expo.push.sound
      end

      def build
        [
          {
            to: to,
            title: title,
            body: body,
            ttl: ttl,
            sound: sound
          }
        ]
      end
    end
  end
end
