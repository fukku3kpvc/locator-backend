if Rails.env.development?
  require 'annotate'
  task :set_annotation_options do
    Annotate.set_defaults(
      'models'                    => 'true',
      'routes'                    => 'false',
      'position_in_routes'        => 'bottom',
      'position_in_class'         => 'bottom',
      'position_in_test'          => 'bottom',
      'position_in_fixture'       => 'bottom',
      'position_in_factory'       => 'bottom',
      'position_in_serializer'    => 'bottom',
      'exclude_tests'             => 'true',
      'exclude_fixtures'          => 'true',
      'exclude_factories'         => 'true',
      'exclude_serializers'       => 'true',
      'exclude_scaffolds'         => 'true',
      'exclude_controllers'       => 'true',
      'exclude_helpers'           => 'true',
    )
  end

  Annotate.load_tasks
end
