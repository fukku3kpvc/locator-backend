module Api
  module Base
    module Helpers
      def render_api_result(payload = nil)
        response_body = status
        if success?
          response_body[:data] = payload if payload
          RL.tagged('API') do
            RL.tagged(filter_hash(request.params).to_s) do
              RL.info(response_body.to_s)
            end
          end
        else
          response_body.merge!(payload) unless payload.empty?
        end

        render json: response_body
      end

      private

      def filter_hash(hash)
        filtered_hash = {}
        hash.each do |key, value|
          filtered_hash[key] = if value.is_a?(Hash)
                                 filter_hash(value)
                               else
                                 check_filter_params(key, value)
                               end
        end
        filtered_hash
      end

      def check_filter_params(key, value)
        if Rails.application.config.filter_parameters.include?(key.to_sym.downcase)
          '[FILTERED]'
        else
          value
        end
      end
    end
  end
end