module Api
  module Base
    module Errors
      def status
        @status ||= Ok.new
        { result: @status.build }
      end

      def success?
        @status.build == 'ok'
      end

      class ActionError < StandardError
        def initialize(result: nil, data: {})
          @result = result
          @data = data
        end

        def to_h
          { result: @result, data: @data }.delete_if { |_, v| v.empty? }
        end
      end

      class Base
        attr_reader :result

        def initialize(*args)
          @result = self.class.name.demodulize.snakecase
          build(*args)
        end

        def build(*_args)
          raise NotImplementedError
        end

        def to_hash
          instance_variables.inject({}) do |memo, var|
            memo[var.to_s.gsub('@', '')] = instance_variable_get(var)
            memo
          end.deep_symbolize_keys!
        end

        alias to_h to_hash
      end

      class Ok < Base
        def build
          @result
        end
      end

      class WrongFormat < Base
        attr_reader :reason

        def build(field, reason, description: nil)
          @data = {
            text: I18n.t!('errors.wrong_format', locale: :ru),
            field: field,
            description: description || reason
          }
        end
      end

      class ValidationFailed < Base
        def build(field, reason, description: nil)
          @data = {
            text: I18n.t!('errors.validation_failed', locale: :ru),
            field: field,
            description: description || reason
          }
        end
      end

      class WrongData < Base
        def build
          @data = { description: I18n.t!('errors.wrong_data', locale: :ru) }
        end
      end

      class WrongSessionToken < Base
        def build
          @result
        end
      end

      class WrongID < Base
        def build
          @result
        end
      end
    end
  end
end