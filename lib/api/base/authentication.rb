module Api
  module Base
    module Authentication
      extend ActiveSupport::Concern

      AUTH_HEADER = 'SESSION-TOKEN'.freeze

      included do
        before_action :authenticate!
      end

      def authenticate!
        current_user
      end

      def current_user
        @current_user ||= determine_user
      end

      private

      def determine_user
        token = request.headers[AUTH_HEADER]
        raise_session_error unless token

        decoded_token = JWT.decode(token, nil, false)
        user_id = decoded_token.first['user_id']
        raise_session_error unless user_id

        User.find(user_id)
      rescue JWT::DecodeError, ActiveRecord::RecordNotFound
        raise_session_error
      end

      def raise_session_error
        raise Errors::ActionError, Errors::WrongSessionToken.new.to_h
      end
    end
  end
end