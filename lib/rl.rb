module RL
  class << self
    delegate :fatal, :error, :warn, :debug, :tagged, to: :'Rails.logger'

    def tagged_with_uid(*tags)
      uid = SecureRandom.hex[0...8]
      time = Time.now
      time_label = time.strftime('%Y-%m-%d %H:%M:%S %z') + " timing:#{time.to_f}"
      tagged(time_label, uid, *tags) { yield self }
    end

    def info(msg)
      filter_params = Rails.application.config.filter_parameters.map(&:to_s)
      filter_params.each do |param|
        msg = msg.gsub(/("#{param}"=>")([^"]*)/, '\1[FILTERED]')
      end

      Rails.logger.info(msg)
    end
  end
end
