class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.belongs_to :user, null: false
      t.column :point, 'geography(Point,4326)', null: false
      t.column :parsed_address, :jsonb, null: false, default: {}
      t.string :address, null: false
      t.timestamps
    end
  end
end
