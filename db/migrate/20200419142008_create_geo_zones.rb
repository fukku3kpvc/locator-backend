class CreateGeoZones < ActiveRecord::Migration[5.2]
  def change
    create_table :geo_zones do |t|
      t.references :friendship, null: false
      t.references :watcher, null: false
      t.references :target, null: false
      t.column :center, 'geography(Point,4326)', null: false
      t.integer :radius, null: false
      t.string :name, null: false
      t.string :color, null: false
      t.timestamps
    end
  end
end
