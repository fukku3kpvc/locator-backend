class CreateMetrics < ActiveRecord::Migration[5.2]
  def change
    create_table :metrics do |t|
      t.belongs_to :user, null: false
      t.integer :battery, null: false
      t.string :time_zone, null: false
      t.timestamps
    end
  end
end
