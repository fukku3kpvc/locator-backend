class AddPgTrgmExtension < ActiveRecord::Migration[5.2]
  def up
    execute <<-DDL
      CREATE EXTENSION pg_trgm;
    DDL
  end

  def down; end
end
