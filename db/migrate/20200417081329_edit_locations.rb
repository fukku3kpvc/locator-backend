class EditLocations < ActiveRecord::Migration[5.2]
  def change
    remove_column :locations, :point
    add_column :locations, :position, 'geography(Point,4326)', null: false
  end
end
