class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.column :number, :bigint, null: false
      t.string :username, null: false
      t.string :full_name, null: false
      t.string :avatar
      t.string :password_digest, null: false, default: ''
      t.timestamps null: false

      t.index :username, unique: true
      t.index :number, unique: true
    end
  end
end
