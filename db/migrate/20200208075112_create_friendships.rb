class CreateFriendships < ActiveRecord::Migration[5.2]
  def up
    execute <<-DDL
      CREATE TYPE friendship_status AS ENUM (
        'initiated',
        'confirmed',
        'rejected'
      );
    DDL
    create_table :friendships do |t|
      t.references :initiator, null: false
      t.references :recipient, null: false
      t.column :state, :friendship_status, null: false, default: 'initiated'
      t.timestamps
      t.timestamp :available_at
      t.index %I[initiator_id recipient_id], unique: true
    end
  end

  def down
    execute <<-DDL
      DROP TYPE IF EXISTS friendship_status;
    DDL
    drop_table :friendships
  end
end
