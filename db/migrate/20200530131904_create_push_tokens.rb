class CreatePushTokens < ActiveRecord::Migration[5.2]
  def up
    execute <<-DDL
      CREATE TYPE device_os AS ENUM (
        'ios',
        'android'
      );
    DDL

    create_table :push_tokens do |t|
      t.belongs_to :user, null: false
      t.column :os, :device_os, null: false
      t.string :token, null: false
      t.timestamps
    end
  end

  def down
    execute <<-DDL
      DROP TYPE IF EXISTS device_os;
    DDL

    drop_table :push_tokens
  end
end
